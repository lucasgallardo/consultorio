<?php
session_start();
include ("conexion.php");
?>
<html>
    <head>
        <title>Detalles</title>
        <script src="js/jquery-2.2.3.min.js"></script>
        <script>
            $(function () {
                $("#guardaExamen").click(function () {
                    var url = "colposcopiaAjax.php";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#formularioColposcopia").serialize(),
                        success: function (data)
                        {
                            $("#resultadoExamen").html(data);
                            $("#formularioColposcopia").clearQueue();
                        }
                    });
                    return false;
                });
            });
        </script>
    </head>
    <body>
        <?php include('navbar2.php') ?>
        <?php include('sidebar2.php') ?>
        <?php include ('operaciones.php'); ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="home.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                    <li><a href="pacientes.php">Pacientes</a></li>
                    <li class="active">Detalles del Paciente</li>
                </ol>
            </div><!--/.row-->
            <br />
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        $id = $_REQUEST['accion'];
                        $paciente = armaPaciente($conexion, $id);
                        ?>
                        <h2 style="color:rgb(48, 165, 255)"><img src="img/icons/editar2.png"> <u>Paciente:</u> <span style="background-color:yellow"><?php echo $paciente['apellido_paciente'] . " " . $paciente['nombre_paciente']; ?></span></h2>

                    </div>

                    <div class="col-md-11">
                        <div class="panel panel-default">
                            <div class="panel-body tabs">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab">Datos Personales</a></li>
                                    <li><a href="#tab2" data-toggle="tab">Antecedentes personales</a></li>
                                    <li><a href="#tab3" data-toggle="tab">Antec. Gineco-obstetricos</a></li>
                                    <li><a href="#tab4" data-toggle="tab">Examen físico</a></li>
                                    <li><a href="#tab5" data-toggle="tab">Ficha obstetrica</a></li>
                                </ul>

                                <div class="tab-content formulario">
                                    <div class="tab-pane fade in active" id="tab1">
                                        <form action="func_pacienteNuevo.php" method="POST">
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label><strong class="text-forms-pacientesEdit">Apellido: </strong></label><input class="form-control" type="text" name="apellido_paciente" value="<?php echo $paciente['apellido_paciente'] ?>"/>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label><strong class="text-forms-pacientesEdit">Nombre: </strong></label><input class="form-control" type="text" name="nombre_paciente" value="<?php echo $paciente['nombre_paciente']; ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientesEdit">Fecha de nacimiento: </strong></label><input class="form-control" type="date" name="nacimiento_pac" value="<?php echo $paciente['fecha_nacimiento'] ?>"/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientesEdit">Edad: </strong></label><input class="form-control" type="text" name="edad_paciente" value="<?php echo $paciente['edad_paciente'] ?>"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <label><strong class="text-forms-pacientesEdit">DNI: </strong></label>
                                                    <input class="form-control" type="text" name="dni_paciente" value="<?php echo $paciente['dni_paciente'] ?>"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <label><strong class="text-forms-pacientesEdit">Teléfono: </strong></label>
                                                    <input class="form-control" type="text" name="tel_paciente" value="<?php echo $paciente['telefono_paciente'] ?>"/>
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label><strong class="text-forms-pacientesEdit">Profesión: </strong></label>
                                                    <input class="form-control" type="text" name="prof_paciente" value="<?php echo $paciente['profesion_paciente'] ?>"/>
                                                </div>
                                                <div class="col-md-1">

                                                </div>
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientesEdit">Empresa: </strong></label>
                                                    <input class="form-control" type="text" name="empresa_paciente" value="<?php echo $paciente['empresa_paciente'] ?>"/>
                                                </div>
                                            </div><br/>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientesEdit">Obra Social: </strong></label>
                                                    <input class="form-control" type="text" name="osocial_paciente" value="<?php echo $paciente['obra_social_paciente'] ?>"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientesEdit">Plan: </strong></label>
                                                    <input class="form-control" type="text" name="plan_osocial_pac" value="<?php echo $paciente['plan_obra_social_paciente'] ?>"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <label><strong class="text-forms-pacientesEdit">Número de Obra social: </strong></label>
                                                    <input class="form-control" type="text" name="nro_osocial_pac" value="<?php echo $paciente['numero_obra_social_paciente'] ?>"/>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientesEdit">Dirección: </strong></label>
                                                    <input class="form-control" type="text" name="dire_paciente" value="<?php echo $paciente['direccion_paciente'] ?>"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientesEdit">Contacto: </strong></label>
                                                    <input class="form-control" type="text" name="contacto_paciente" value="<?php echo $paciente['contacto_paciente'] ?>"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientesEdit">E-mail: </strong></label>
                                                    <input class="form-control" type="text" name="email_paciente" value="<?php echo $paciente['mail_paciente'] ?>"/>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientesEdit">Grupo sanguineo: </strong></label>
                                                    <input class="form-control" type="text" name="GS_paciente" value="<?php echo $paciente['grupo_sanguineo_paciente'] ?>"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientesEdit">Fecha de 1ra consulta: </strong></label>
                                                    <input class="form-control" type="date" name="fecha_1_consulta_paciente" value="<?php echo $paciente['fecha_primera_consulta_paciente'] ?>"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientesEdit">Estado civil: </strong></label>
                                                    <select class="form-control" name="estado_civil_paciente" >
                                                        <option><?php echo $paciente['estado_civil_paciente'] ?></option>
                                                        <option class="disable">-----</option>
                                                        <option>Divorciada</option>
                                                        <option>Viuda</option>
                                                        <option>Concubinato</option>
                                                        <option>Soltera</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label><strong class="text-forms-pacientesEdit">Motivo de consulta: </strong></label>
                                                    <textarea class="form-control" rows="3" name="motivo_paciente"><?php echo $paciente['motivo_consulta'] ?></textarea>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label><strong class="text-forms-pacientesEdit">Notas adicionales</strong></label>
                                                    <textarea class="form-control" rows="3" name="notas_adicionales_paciente"><?php echo $paciente['notas'] ?></textarea>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input name="guardar" type="submit" class="btn btn-warning btn-lg hoverable" value="Guardar Cambios"/>
                                                </div>
                                            </div>

                                        </form>
                                    </div>

                                    <!--***********************************************************************************************-->
                                    <div class="tab-pane fade" id="tab2">
                                        <form action="" method="POST">
                                            <div class="row"><br>
                                                <div class="col-md-6">
                                                    <input type="hidden" value="<?php echo $paciente['id_antecedente'] ?>">
                                                    <div class="checkbox">
                                                        <label>
                                                            <?php if ($paciente['diabetes'] == "on") {
                                                                ?>
                                                                <input name="ch_pac_diab" type="checkbox" checked="checked"> <strong class="text-forms-pacientes" style="color:springgreen">Diabetes </strong>
                                                            <?php } else { ?>
                                                                <input name="ch_pac_diab" type="checkbox"> <strong class="text-forms-pacientes">Diabetes </strong>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <?php if ($paciente['hipertension'] == "on") {
                                                                ?>
                                                                <input name="ch_pac_hiper" type="checkbox" checked="checked"> <strong class="text-forms-pacientes" style="color:springgreen">Hipertensión </strong>
                                                            <?php } else { ?>
                                                                <input name="ch_pac_hiper" type="checkbox"> <strong class="text-forms-pacientes">Hipertensión </strong>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <?php if ($paciente['hipotiroidismo'] == "on") {
                                                                ?>
                                                                <input name="ch_pac_hipo" type="checkbox" checked="checked"> <strong class="text-forms-pacientes" style="color:springgreen">Hipotiroidismo </strong>
                                                            <?php } else { ?>
                                                                <input name="ch_pac_hipo" type="checkbox"> <strong class="text-forms-pacientes">Hipotiroidismo </strong>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <?php if ($paciente['tabaquismo'] == "on") {
                                                                ?>
                                                                <input name="ch_pac_tabaq" type="checkbox" checked="checked"> <strong class="text-forms-pacientes" style="color:springgreen">Tabaquismo </strong>
                                                            <?php } else { ?>
                                                                <input name="ch_pac_tabaq" type="checkbox"> <strong class="text-forms-pacientes">Tabaquismo </strong>
                                                            <?php } ?>
                                                        </label>
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="col-md-6">
                                                        <label><strong class="text-forms-pacientes">Peso: </strong></label>
                                                        <input class="form-control" type="text" name="peso_pac" value="<?php echo $paciente['peso'] ?> "/>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label><strong class="text-forms-pacientes">Altura: </strong></label>
                                                        <input class="form-control" type="text" name="altura_pac" value="<?php echo $paciente['altura'] ?> "/>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label><strong class="text-forms-pacientesEdit">IMC: </strong></label>
                                                        <input class="form-control" type="text" name="imc_pac" value="<?php echo $paciente['imc'] ?> "/>
                                                    </div>
                                                </div>
                                            </div><br><!--Cierro row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientesEdit">Antecedentes Alérgicos: </strong></label>
                                                    <textarea class="form-control" rows="4" name="ant_alergicos_pac"><?php echo $paciente['antecedentes_alergico'] ?> 
                                                    </textarea>
                                                </div>
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientesEdit">Antecedentes Clínicos: </strong></label>
                                                    <textarea class="form-control" rows="4" name="ant_clinicos_pac"> <?php echo $paciente['antecedentes_clinicos'] ?> </textarea>
                                                </div>
                                            </div><br><!--Cierro row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientesEdit">Antecedentes Quirúrgicos: </strong></label>
                                                    <textarea class="form-control" rows="4" name="ant_quir_pac"> <?php echo $paciente['antecedentes_quirurgicos'] ?></textarea>
                                                </div>
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientesEdit">Fármacos: </strong></label>
                                                    <textarea class="form-control" rows="4" name="farmacos_pac"> <?php echo $paciente['farmacos'] ?></textarea>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientes">Antecedentes Heredo-Familiares: </strong></label><br>
                                                    <div class="form-group">
                                                        <label class="sr-only" ></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">Madre: </div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" value="<?php echo $paciente['madre'] ?>" name="madre_pac">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="sr-only" ></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">Padre: </div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" value="<?php echo $paciente['padre'] ?>" name="padre_pac">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="sr-only" ></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">Otros: </div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" value="<?php echo $paciente['otros_antecedentes'] ?>" name="otros_pac">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientesEdit">Notas: </strong></label>
                                                    <br>
                                                    <textarea class="form-control" rows="6" name="notas_heredo_fam_pac"><?php echo $paciente['notas_antecedentes'] ?></textarea>
                                                </div>
                                                <input type="hidden" value="<?php echo $paciente['idPaciente_personales'] ?>">
                                            </div><!--Cierro row--><br>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="submit" class="btn btn-warning btn-lg btn-block hoverable" name="guardar2" value="Guardar Cambios"/>
                                                </div>
                                                <div class="col-md-9"></div>
                                            </div><br><!--Cierro row-->
                                        </form>
                                    </div>
                                    <!--***********************************************************************************************-->
                                    <div class="tab-pane fade" id="tab3">
                                        <form action="" method="POST">
                                            <div class="row"><br>
                                                <input type="hidden" value="<?php echo $paciente['id_ginecoObs'] ?>">
                                                <div class="col-md-4 form-inline">
                                                    <div class="form-group">
                                                        <label for="exampleInputName2"><strong class="text-forms-pacientes">Menarca: </strong></label>
                                                        <input type="text" class="form-control" id="exampleInputName2" value="<?php echo $paciente['menarca_pac'] ?>" name="menarca_pac">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 form-inline">
                                                    <div class="form-group">
                                                        <label for="exampleInputName2"><strong class="text-forms-pacientesEdit">RM: </strong></label>
                                                        <input type="text" class="form-control" id="exampleInputName2" value="<?php echo $paciente['rm_pac'] ?>" name="rm_pac">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 form-inline">
                                                    <div class="form-group">
                                                        <label for="exampleInputName2"><strong class="text-forms-pacientesEdit">Menopausia: </strong></label>
                                                        <input type="text" class="form-control" id="exampleInputName2" value="<?php echo $paciente['menopausia_pac'] ?>" name="menopausia_pac">
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientesEdit">Gestas: </strong></label>
                                                        <input class="form-control" type="text" name="gestas_pac" value="<?php echo $paciente['gestas_pac'] ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientesEdit">Partos: </strong></label>
                                                        <input class="form-control" type="text" name="partos_pac" value="<?php echo $paciente['partos_pac'] ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientesEdit">CS: </strong></label>
                                                        <input class="form-control" type="text" name="cs_pac" value="<?php echo $paciente['cs_pac'] ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientesEdit">AB: </strong></label>
                                                        <input class="form-control" type="text" name="ab_pac" value="<?php echo $paciente['ab_pac'] ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientesEdit">EE: </strong></label>
                                                        <input class="form-control" type="text" name="ee_pac" value="<?php echo $paciente['ee_pac'] ?>"/>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientesEdit">FUM: </strong></label>
                                                        <input class="form-control" type="date" name="" value="<?php echo $paciente['fum_pac'] ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientesEdit">Dismenorrea (si / no): </strong></label>
                                                        <input class="form-control" type="text" name="dismenorrea_pac" value="<?php echo $paciente['dismenorrea_pac'] ?>"/>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>
                                                        <strong class="text-forms-pacientes">Anticoncepción: </strong>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <input class="form-control" type="text" name="tipo_anticoncep_pac" value="Tipo: <?php echo $paciente['tipo_anticoncep_pac'] ?>"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input class="form-control" type="text" name="tiempo_anticoncep_pac" value="Tiempo: <?php echo $paciente['tiempo_anticoncep_pac'] ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <textarea class="form-control" rows="3" name="notas_anticoncep_pac" value="<?php echo $paciente['notas_anticoncep_pac'] ?>"></textarea>
                                                    </div>
                                                </div>
                                            </div><br><!--Cierra row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientesEdit">Cirugías Ginecológicas: </strong></label>
                                                        <textarea class="form-control" rows="3" name="cirug_ginec_pac"><?php echo $paciente['cirug_ginec_pac'] ?></textarea>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="submit" class="btn btn-success btn-lg btn-block hoverable" name="guardar3" value="Guardar Cambios"/>
                                                </div>
                                                <div class="col-md-9"></div>
                                            </div>
                                        </form>
                                    </div>
                                    <!--********************************************************************************************-->
                                    <div class="tab-pane fade" id="tab4">
                                        <form action="" method="POST">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientes">Fecha: </strong></label>
                                                        <input class="form-control" type="date" name="fecha_ex_fisico_pac">
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientes">MC: </strong></label>
                                                        <input class="form-control" type="tex" name="mc_pac">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row"><br>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientes">Examen Físico: </strong></label>
                                                        <input class="form-control" type="tex" name="ex_fisico_pac">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--                                            <div class="row">
                                            <legend></legend>
                                            <div class="col-md-12">
                                            <div class="form-horizontal">
                                            <div class="form-group">
                                            <label for="colposcopia" class="control-label col-xs-2"> Colposcopia: </label>
                                                                                                                                                    <div class="col-xs-8">
                                                                                                                                                        <input id="colposcopia" type="text" class="form-control" placeholder="" name="colpos_pac">
                                                                                                                                                    </div>
                                                                                                                                                    <a href="" class="btn btn-warning btn-md">?</a>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        <div class="col-md-12">
                                                                                                                                            <div class="form-horizontal">
                                                                                                                                                <div class="form-group">
                                                                                                                                                    <label for="pap" class="control-label col-xs-2"> Pap: </label>
                                                                                                                                                    <div class="col-xs-8">
                                                                                                                                                        <input id="pap" type="text" class="form-control" placeholder="" name="pap_pac">
                                                                                                                                                    </div>
                                                                                                                                                    <a href="" class="btn btn-success btn-md">?</a>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        <div class="col-md-12">
                                                                                                                                            <div class="form-horizontal">
                                                                                                                                                <div class="form-group">
                                                                                                                                                    <label for="" class="control-label col-md-2">Ecografía Ginecológica: </label>
                                                                                                                                                    <div class="col-xs-8">
                                                                                                                                                        <input id="" type="text" class="form-control" placeholder="" name="eco_ginec_pac">
                                                                                                                                                    </div>
                                                                                                                                                    <a href="" class="btn btn-primary btn-md">?</a>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        <div class="col-md-12">
                                                                                                                                            <div class="form-horizontal">
                                                                                                                                                <div class="form-group">
                                                                                                                                                    <label for="" class="control-label col-xs-2">Ecografía Mamaria: </label>
                                                                                                                                                    <div class="col-xs-8">
                                                                                                                                                        <input id="" type="text" class="form-control" placeholder="" name="eco_mam_pac">
                                                                                                                                                    </div>
                                                                                                                                                    <a href="" class="btn btn-info btn-md">?</a>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        <div class="col-md-12">
                                                                                                                                            <div class="form-horizontal">
                                                                                                                                                <div class="form-group">
                                                                                                                                                    <label for="" class="control-label col-xs-2">Mamografía: </label>
                                                                                                                                                    <div class="col-xs-8">
                                                                                                                                                        <input id="" type="text" class="form-control" placeholder="" name="mamograf_pac">
                                                                                                                                                    </div>
                                                                                                                                                    <a href="" class="btn btn-danger btn-md">?</a>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        <legend></legend>
                                                                                                                                    </div>
                                                                                                                                    <div>-->

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#colposcopia" aria-controls="home" role="tab" data-toggle="tab">Colposcopia</a></li>
                                                <li role="presentation"><a href="#pap" aria-controls="profile" role="tab" data-toggle="tab">Pap</a></li>
                                                <li role="presentation"><a href="#ginecologica" aria-controls="messages" role="tab" data-toggle="tab">Ecografía Ginecológica</a></li>
                                                <li role="presentation"><a href="#mamaria" aria-controls="settings" role="tab" data-toggle="tab">Ecografía Mamaria</a></li>
                                                <li role="presentation"><a href="#mamografia" aria-controls="settings" role="tab" data-toggle="tab">Mamografía</a></li>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="colposcopia">
                                                    <table class="table table-hover table-bordered table-condensed">
                                                        <thead>
                                                        <th class="info">Fecha</th>
                                                        <th class="info">Datos</th>
                                                        <th class="info">Agregar</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                        <form method="POST" id="formularioColposcopia">
                                                            <td><input class="form-control" type="date" id="fechaColposcopia" name="fechaColposcopia"></td>
                                                            <td><input class="form-control" type="text" name="datosColposcopia"></td>
                                                            <input type="hidden" class="form-control" id="idPaciente" placeholder="" name="id_paciente" value= "<?php echo $id; ?>" >
                                                            <td>
                                                                <input type="button" value="Agregar" id="guardaExamen" class="btn btn-success active">
                                                            </td>
                                                        </form>
                                                        </tr>
                                                        <tr>
                                                            <?php echo cargaColposcopia($conexion, $id); ?>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div id="resultadoExamen"></div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="pap">
                                                    <table class="table table-hover table-bordered table-condensed">
                                                        <thead>
                                                        <th class="warning">Fecha</th>
                                                        <th class="warning">Datos</th>
                                                        <th class="warning">Agregar</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><input class="form-control" type="date" id="fechaPap" name="fechaPap"></td>
                                                                <td><input class="form-control" type="text" name="datosPap"></td>
                                                        <input type="hidden" class="form-control" id="idPaciente" placeholder="" name="id_paciente" value= "<?php echo $paciente['id_paciente']; ?>" >
                                                        <td>
                                                            <input type="button" value="Agregar" id="guardaExamen" class="btn btn-success active">
                                                        </td> 
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="ginecologica">
                                                    <table class="table table-hover table-bordered table-condensed">
                                                        <thead>
                                                        <th class="success">Fecha</th>
                                                        <th class="success">Datos</th>
                                                        <th class="success">Agregar</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><input class="form-control" type="date" id="fechaGinecologica" name="fechaGinecologica"></td>
                                                                <td><input class="form-control" type="text" name="datosGinecologica"></td>
                                                        <input type="hidden" class="form-control" id="idPaciente" placeholder="" name="id_paciente" value= "<?php echo $paciente['id_paciente']; ?>" >
                                                        <td>
                                                            <input type="button" value="Agregar" id="guardaExamen" class="btn btn-success active">
                                                        </td> 
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="mamaria">
                                                    <table class="table table-hover table-bordered table-condensed">
                                                        <thead>
                                                        <th class="info">Fecha</th>
                                                        <th class="info">Datos</th>
                                                        <th class="info">Agregar</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><input class="form-control" type="date" id="fechaMamaria" name="fechaMamaria"></td>
                                                                <td><input class="form-control" type="text" name="datosMamaria"></td>
                                                        <input type="hidden" class="form-control" id="idPaciente" placeholder="" name="id_paciente" value= "<?php echo $paciente['id_paciente']; ?>" >
                                                        <td>
                                                            <input type="button" value="Agregar" id="guardaExamen" class="btn btn-success active">
                                                        </td> 
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="mamografia">
                                                    <table class="table table-hover table-bordered table-condensed">
                                                        <thead>
                                                        <th class="info">Fecha</th>
                                                        <th class="info">Datos</th>
                                                        <th class="info">Agregar</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><input class="form-control" type="date" id="fechaMamografia" name="fechaMamografia"></td>
                                                                <td><input class="form-control" type="text" name="datosMamografia"></td>
                                                        <input type="hidden" class="form-control" id="idPaciente" placeholder="" name="id_paciente" value= "<?php echo $paciente['id_paciente']; ?>" >
                                                        <td>
                                                            <input type="button" value="Agregar" id="guardaExamen" class="btn btn-success active">
                                                        </td> 
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientes">Otros: </strong></label>
                                                        <textarea class="form-control" rows="5" name="otros_ex_fis_pac" placeholder=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><strong class="text-forms-pacientes">Notas: </strong></label>
                                                        <textarea class="form-control" rows="5" name="notas_ex_fis_pac" placeholder=""></textarea>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="submit" class="btn btn-info btn-lg btn-block hoverable" name="guardar4" value="Guardar"/>
                                                </div>
                                                <div class="col-md-9"></div>
                                            </div>
                                        </form>
                                    </div><!--IMPORTANT-->

                                    <!--******************************************************************************************-->
                                    <div class="tab-pane fade" id="tab5">

                                    </div>
                                    <!--******************************************************************************************-->
                                </div>
                            </div>
                        </div><!--/.panel-->
                    </div><!--/.col-->
                </div><!--cierra row-->
            </div><!--cierra container-->
        </div>

        <script src="bootstrap/js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap-table.js"></script>
        <script src="js/materialize.min.js"></script>
    </body>
</htm>

