<?php
    session_start();
    include ("conexion.php");
    if (isset($_POST['s'])){
    $busca = mysqli_real_escape_string($conexion,$_POST['s']);
    } 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Valentin</title>

        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/datepicker3.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap-table.css" rel="stylesheet">
        <link href="bootstrap/css/styles.css" rel="stylesheet">
        <link rel="stylesheet" href="bootstrap/css/hover.css">


        <!--Icons-->
        <script src="bootstrap/js/lumino.glyphs.js"></script>

        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
     <?php include('navbar2.php'); ?>
        <?php include('sidebar2.php'); ?>


        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main "> <!--MAS IMPORTANTE-->		
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="home.php" class="hvr-pulse"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                    <li class="active">Pacientes</li>
                </ol>
            </div><!--/.row-->
            <hr>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <a href="paciente_nuevo_datos_personal.php" class="btn btn-default hvr-pulse" style="border:1px solid">
                            <i class="glyphicon glyphicon-plus-sign"></i> &nbsp Agregar nuevo paciente
                        </a>
                       
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-11">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="col-md-6">
                                    <h2><a href="pacientes.php" class="link-sin-decor">Pacientes</a></h2>
                                </div>
                                <div class="col-md-6 col-xs-10">
                                   	<form method="post" class="navbar-form navbar-right" role="search" action="">
                                      <div class="form-group">
                                        <input type="text" name="s" class="form-control" placeholder="Buscar">
                                      </div>
                                      <button type="submit" class="btn btn-success" ><i class="glyphicon glyphicon-search"></i> Search</button>
                                      
                                    </form>                                         
                                </div>
                            </div>
                            <div class="panel-body">
                                    <!--<table data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-pagination="true" data-sort-name="name" data-sort-order="desc">-->
                                <table class="table table-hover table-bordered table-responsive" data-pagination="true">
                                    <thead>
                                        <tr class="info">
                                            <th >Apellido</th>
                                            <th >Nombre</th>
                                            <th >Fecha de Nacimiento</th>
                                            <th >Edad</th>
                                            <th >DNI</th>
                                            <th >Teléfono</th>
                                            <th >Obra Social</th>
                                            <th>Orden Consulta</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    if(empty($busca)){
                                         $ask = mysqli_query($conexion, "SELECT id_paciente,apellido_pac,nombre_pac,fecha_nacimiento,edad_pac,dni_pac,tel_paciente,obsocial_pac from pacientes order by apellido_pac limit 10") or die(mysqli_error($conexion));
                                    }                   
                                    else{
                                        $ask= mysqli_query($conexion,"select * from pacientes where apellido_pac like '%".$busca."%' or nombre_pac like '%".$busca."%' or fecha_nacimiento like '%".$busca."%' or edad_pac like '%".$busca."%' or dni_pac like '%".$busca."%' or obsocial_pac like '%".$busca."%' ");
                                    }
                                        
                                    while ($traer = mysqli_fetch_array($ask)) {
                                        $id = $traer['id_paciente'];
                                        $apellido = $traer['apellido_pac']
                                        ?>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <?php echo "<a href='detallesPaciente2.php?accion=$id'> $apellido </a> "?>
                                                </td>
                                                <td><?php print $traer['nombre_pac'] ?></td>
                                                <td><?php print $traer['fecha_nacimiento'] ?></td>
                                                <td><?php print $traer['edad_pac'] ?></td>
                                                <td><?php print $traer['dni_pac'] ?></td>
                                                <td><?php print $traer['tel_paciente'] ?></td>
                                                <td><?php print $traer['obsocial_pac'] ?></td>
                                                <td><?php  echo "<a href='php/imprimirOrdenConsulta.php?accion=$id'><button class='btn btn-success btn-xs '><span class='glyphicon glyphicon-print'></span> Orden de Consulta</button></a>";?></td>
                                            </tr>
                                            
                                        </tbody>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->	

                <div class="left"><a href="" data-toggle="modal" data-target="#squarespaceModal"><img src="img/icons/1447531440_Warning.png" alt=""> Necesita ayuda?</a></div>
                <div class="modal fade alert" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
                                <h3 class="modal-title" id="lineModalLabel">Necesita ayuda?</h3>
                            </div>
                            <div class="modal-body">
                                <!--Contenido del modal -->
                            </div>
                        </div>
                    </div>
                </div>

            </div><!--/.container-->

        </div>	<!--/.main-->





        <!-------------------------------------------------------------------------------->


        <script src="bootstrap/js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap-table.js"></script>
        <script src="js/jquery-2.2.3.min.js"></script>
        <script src="js/materialize.min.js"></script>
    </body>

</html>

