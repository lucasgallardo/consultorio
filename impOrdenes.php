<?php
    session_start();
    include ("conexion.php");
    if (isset($_POST['s'])){
    $busca = mysqli_real_escape_string($conexion,$_POST['s']);
    } 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Valentin</title>

        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/datepicker3.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap-table.css" rel="stylesheet">
        <link href="bootstrap/css/styles.css" rel="stylesheet">
        <link rel="stylesheet" href="bootstrap/css/hover.css">


        <!--Icons-->
        <script src="bootstrap/js/lumino.glyphs.js"></script>

        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <style>
            table{
               text-align: center;
            }
        </style>
    </head>

    <body>
     <?php include('navbar2.php'); ?>
        <?php include('sidebar2.php'); ?>


        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main "> <!--MAS IMPORTANTE-->		
            <div class="row">
                <ol class="breadcrumb">
                    <li  id="rotacion"><a href="home.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                    <li><a href="pacientes.php">Pacientes</a></li>
                    <li class="active">Ordenes a imprimir</li>
                </ol>
            </div><!--/.row-->
            <div class="container">
                <br>
                <div class="row">
                    <div class="col-lg-11">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="col-md-6 col-xs-3">
                                    <h2><a href="impOrdenes.php" class="link-sin-decor"><img src="img/icons/printers & Faxes (3).png" alt=""> Ordenes a imprimir</a></h2>
                                </div>
                                <div class="col-md-6 col-xs-10">
                                   	<form method="post" class="navbar-form navbar-right" role="search" action="">
                                      <div class="form-group">
                                        <input type="text" name="s" class="form-control" placeholder="Buscar por DNI">
                                      </div>
                                      <button type="submit" class="btn btn-success hvr-float-shadow" ><i class="glyphicon glyphicon-search "></i> Search</button>
                                      
                                    </form>                                         
                                </div>
                            </div>
                            <div class="panel-body" >
                                    <!--<table data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-pagination="true" data-sort-name="name" data-sort-order="desc">-->
                                <table class="table table-hover table-bordered table-responsive" data-pagination="true">
                                    <thead>
                                        <tr class="success" >
                                            <th style="text-align:center">Apellido</th>
                                            <th style="text-align:center">Nombres</th>
                                            <th style="text-align:center">DNI</th>
                                            <th style="text-align:center">O.Consulta</th>
                                            <th style="text-align:center">PAP</th>
                                            <th style="text-align:center">Analíticas</th>
                                            <th style="text-align:center">Embarazo</th>
                                            <th style="text-align:center">Ginecológicos</th>
                                            <th style="text-align:center">Otros</th>
                                            <th style="text-align:center">Recetario</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    if(empty($busca)){
                                         $ask = mysqli_query($conexion, "SELECT id_paciente,apellido_pac,nombre_pac,fecha_nacimiento,edad_pac,dni_pac,tel_paciente,obsocial_pac from pacientes order by apellido_pac limit 1") or die(mysqli_error($conexion));
                                    }                   
                                    else{
                                        $ask= mysqli_query($conexion,"select * from pacientes where apellido_pac like '%".$busca."%' or nombre_pac like '%".$busca."%' or fecha_nacimiento like '%".$busca."%' or edad_pac like '%".$busca."%' or dni_pac like '%".$busca."%' or obsocial_pac like '%".$busca."%' ");
                                    }
                                        
                                    while ($traer = mysqli_fetch_array($ask)) {
                                        $id = $traer['id_paciente'];
                                        ?>
                                        <tbody>
                                            <tr>
                                                <td><?php print $traer['apellido_pac']?></td>
                                                <td><?php print $traer['nombre_pac'] ?></td>
                                                <td><?php print $traer['dni_pac'] ?></td>
                                                <td><?php  echo "<a href='php/imprimirOrdenConsulta.php?accion=$id' class='hvr-buzz-out'><button class='btn btn-success btn-md'><span class='glyphicon glyphicon-print'></span> Orden</button></a>";?></td>
                                                <td><?php  echo "<a href='php/imprimirPap.php?accion=$id' class='hvr-buzz-out'><button class='btn btn-info btn-md'><span class='glyphicon glyphicon-print'></span> PAP</button></a>";?></td>
                                                <td>
                                                    <div class="dropdown" >
                                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                           <button class="btn btn-default btn-md hvr-underline-from-left3">Analíticas <span class="caret"></span></button>
                                                        </a>
                                                        <ul class="dropdown-menu" role="menu">
                                                        <li><?php  echo "<a href='php/imprimirAnalitica.php?accion=$id'><span class='glyphicon glyphicon-print'></span> Analitica°1</a>"?></li>
                                                        <li><?php echo "<a href='php/imprimirAnalitica2.php?accion=$id'><span class='glyphicon glyphicon-print'></span> Analitica°2</a>" ?></li>
                                                        <li><?php echo "<a href='php/imprimirAnalitica3.php?accion=$id'><span class='glyphicon glyphicon-print'></span> Tipificación</a>" ?></li>
                                                        <li class="divider"></li>
                                                        <li><?php  echo "<a href='php/imprimirDosajeHorm_1.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Dosaje Hormonal 1</a>";?></li>            <li><?php  echo "<a href='php/imprimirDosajeHorm_2.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Marcadores Tumorales</a>";?></li>
                                                        <li><?php  echo "<a href='php/imprimirDosTiroideo.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Dosaje Tiroideo</a>";?></li>
                                                        <li class="divider"></li>
                                                        <li><?php  echo "<a href='php/imprimirUrocultivo.php?accion=$id'><span class='glyphicon glyphicon-print'></span> Urocultivo</a>";?></li>
                                                        <li class="divider"></li>
                                                        <li><?php  echo "<a href='php/imprimirSub_uni_beta.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> SUB-UN-BETA</a>";?></li>
                                                        <li><?php  echo "<a href='php/imprimirTipi.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Tipificación</a>";?></li>
                                                        <li><?php  echo "<a href='php/imprimirTestCoombs.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Test de Coombs</a>";?></li>
                                                        <li><?php  echo "<a href='php/imprimirTrombo.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Trombofilia</a>";?></li>
                                                        <li class="divider"></li>
                                                        <li><?php  echo "<a href='php/impCultivo.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Cultivo de flujo completo</a>";?></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td>
                                                <div>
                                                    <div class="dropdown" >
                                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                           <button class="btn btn-default btn-md hvr-underline-from-left">Embarazo <span class="caret"></span></button>
                                                        </a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><?php  echo "<a href='php/imprimirCTOG.php?accion=$id'><span class='glyphicon glyphicon-print'></span> CTGO</a>";?></li>
                                                            <li><?php  echo "<a href='php/imprimirStrepto.php?accion=$id'><span class='glyphicon glyphicon-print'></span> Strepto</a>";?> </li>
                                                            <li class="divider"></li>
                                                            <li><?php  echo "<a href='php/imprimirEcoObstec1.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Ecografía obstétrica</a>";?></li>
                                                            <li><?php  echo "<a href='php/imprimirEcoObstec2.php?accion=$id'><span class='glyphicon glyphicon-print'></span> Ecografía doppler obstetrica  fetal</a>";?></li>
                                                            <li class="divider"></li>
                                                            <li><?php  echo "<a href='php/imprimirEcoCardioDopler_1.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Eco Dopler </a>";?></li>
                                                            <li><?php  echo "<a href='php/imprimirEcoCardioDopler_2.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> N.S.T</a>";?></li>
                                                            <li><?php  echo "<a href='php/imprimirEcoDoplerArtUterinas.php?accion=$id'><span class='glyphicon glyphicon-print'></span> Ecodoppler Arterias Uterinas</a>";?></li>
                                                            <li class="divider"></li>
                                                            <li><?php  echo "<a href='php/imprimirCursoPreparto.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Curso Preparto</a>";?></li>
                                                            <li><?php  echo "<a href='php/imprimirClearance.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Clearance</a>";?></li>
                                                            <li class="divider"></li>
                                                            <li><?php  echo "<a href='php/imprimirScreening.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> SCREENING</a>";?></li>
                                                            <li><?php  echo "<a href='php/impVacuna.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Vacuna</a>";?></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                </td>
                                                <td>
                                                    <div class="dropdown" >
                                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                           <button class="btn btn-default btn-md hvr-underline-from-left3">Ginecológicos <span class="caret"></span></button>
                                                        </a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><?php  echo "<a href='php/imprimirECG.php?accion=$id' class=''><span class='glyphicon glyphicon-print'></span> ECG</a>";?></li>
                                                            <li><?php  echo "<a href='php/imprimirDensiom.php?accion=$id' class=''><span class='glyphicon glyphicon-print'></span> DMO </a>";?></li>
                                                            <li class="divider"></li>
                                                            <li><?php  echo "<a href='php/imprimirEcoGinec.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Eco Ginec. transvaginal</a>";?></li>
                                                            <li><?php  echo "<a href='php/imprimirEcoGinec2.php?accion=$id'><span class='glyphicon glyphicon-print'></span> Ecografía Ginecológica</a>";?></li>
                                                            <li class="divider"></li>
                                                            <li><?php  echo "<a href='php/imprimirEcoMamo1.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Mamografía bilateral</a>";?></li>
                                                            <li><?php  echo "<a href='php/imprimirEcoMamo2.php?accion=$id'><span class='glyphicon glyphicon-print'></span> Ecografía Mamaria Bilateral</a>";?></li>
                                                            <li class="divider"></li>
                                                            
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="dropdown" >
                                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                           <button class="btn btn-default btn-md hvr-underline-from-left">Otros <span class="caret"></span></button>
                                                        </a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><?php  echo "<a href='php/imprimirOrdenInternacion.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Orden de Internación</a>";?></li>
                                                            <li><?php  echo "<a href='php/imprimirPreAnestesica.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Visita Pre Anestesica</a>";?></li>
                                                            <li><?php  echo "<a href='php/impEcoRenal.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Eco Renal</a>";?></li>
                                                            <li><?php  echo "<a href='php/impEcoTiroides.php?accion=$id'><span class='glyphicon glyphicon-print' id='rotacion'></span> Eco Tiroides</a>";?></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td><?php  echo "<a href='php/imprimirRecetario.php?accion=$id' class='hvr-buzz-out'><button class='btn btn-warning btn-md '><span class='glyphicon glyphicon-print'></span> Recetario</button></a>";?></td>
                                                                                           
                                            </tr>
                                        </tbody>    
                                        <?php
                                }
                                ?>                              
                                </table>
                                    
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->	

            </div><!--/.container-->

        </div>	<!--/.main-->





        <!-------------------------------------------------------------------------------->


        <script src="bootstrap/js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap-table.js"></script>
        <script src="js/jquery-2.2.3.min.js"></script>
        <script src="js/materialize.min.js"></script>
    </body>

</html>

