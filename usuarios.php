<?php
    session_start();
    include ("conexion.php");

?>
<link rel="stylesheet" href="bootstrap/css/hover.css"/>
<?php
include "inc/head.inc";
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
    <br><br>
       <div class="container">
        <div class="row">
            <div class="col-md-11">
                <div class="panel panel-default hoverable">
                    <div class="panel-heading dark-overlay "><h2>Bienvenido al sector de Usuarios</h2></div>
                      <div class="panel-body" style="color:black">
<!-- Button trigger modal -->
  <a data-toggle="modal" href="#myModal" class="btn btn-default btn-lg hvr-underline-from-left">+<i class="glyphicon glyphicon-user"></i> Agregar Usuario</a>
<br><br>
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Complete los campos para agregar un usuario</h4>
        </div>
        <div class="modal-body">
			<form role="form" method="post" action="php/agregar.php">
			  <div class="form-group">
				<label for="name">Usuario</label>
				<input type="text" class="form-control" name="name" required>
			  </div>
			  <div class="form-group">
				<label for="lastname">E-mail usuario</label>
				<input type="text" class="form-control" name="mail" required>
			  </div>
			  <div class="form-group">
				<label for="address">Contraseña</label>
				<input type="text" class="form-control" name="pass" required>
			  </div>
			  <div class="form-group">
				<label for="phone">Tipo usuario</label>
				<input type="text" class="form-control" name="tipo" >
			  </div>
			  <input type="submit" name="submit" value="Aregar" class="btn btn-info">
			</form>
        </div>

      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

                       <table class="table table-hover table-bordered table-responsive">
                <thead>
                    <tr class="warning">
                        <th>Nombre Usuario</th>
                        <th>Email Usuario</th>
                        <th>Contraseña Usuario</th>
                        <th>Tipo de usuario</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <?php
                $usuarios = mysqli_query($conexion, "select * from usuarios") or die(mysqli_error($conexion));
                
                    while ($sql=mysqli_fetch_array($usuarios)){
                        $id = $sql['id_usuario'];
                ?>
                <tbody>
                    <tr>
                        <td><?php print $sql['usuario_nombre']; ?></td>
                        <td><?php print $sql['usuario_email']; ?></td>
                        <td><?//php print $sql['usuario_contra']; ?>**********</td>
                        <td><?php print $sql["tipo_user"]; ?></td>
                        <td style="width:150px;">
                            <a href="#" class="btn btn-sm btn-primary">Editar</a>
                            <?php  echo "<a class='btn btn-danger btn-sm' href='php/eliminarUser.php?accion=$id'>Eliminar</a>";?>
                        </td>
                    </tr>
                </tbody>
                <?php
                    }
                ?>
            </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11">
                
            </div>
        </div>
    </div>
</div>
	<script src="bootstrap/js/jquery-1.11.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/chart.min.js"></script>
	<script src="bootstrap/js/chart-data.js"></script>
	<script src="bootstrap/js/easypiechart.js"></script>
	<script src="bootstrap/js/easypiechart-data.js"></script>
	<script src="bootstrap/js/bootstrap-datepicker.js"></script>
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="js/materialize.min.js"></script>