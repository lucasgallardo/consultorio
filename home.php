<?php
    session_start();
    include ("conexion.php");
    if(isset($_SESSION['usuario_nombre'])){  
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Valentin</title>

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/css/datepicker3.css" rel="stylesheet">
<link href="bootstrap/css/styles.css" rel="stylesheet">
<link rel="stylesheet" href="bootstrap/css/hover.css">

<!--Icons-->
<script src="bootstrap/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<?php include ('navbar2.php') ?>
	<?php include ('sidebar2.php') ?>

		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main "> <!--MAS IMPORTANTE-->		
		<div class="row">
			<ol class="breadcrumb ">
				<li><a href="home.php" class="hoverable"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Home</li>
			</ol>
		</div><!--/.row-->
		<hr>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h1 style="color:#ff9800">Bienvenida Doctora.</h1>
					<br>
					<h3>Por favor seleccione una pestaña para continuar.</h3>
					<hr>
				</div>
					<a href="pacientes.php">
						<div class="col-xs-6 col-md-3">
							<div class="panel panel-blue hoverable">
								<div class="panel-heading dark-overlay">Pacientes</div>
								<div class="panel-body">
									<p>Ingrese aquí para administrar pacientes.</p>
								</div>
							</div>
						</div>
					</a>

					<a href="#">
						<div class="col-xs-6 col-md-3">
							<div class="panel panel-teal hvr-float-shadow">
								<div class="panel-heading dark-overlay">Turnos</div>
								<div class="panel-body">
									<p>Ingrese aquí para administrar los turnos.</p>
								</div>
							</div>
						</div>
					</a>

					<a href="usuarios.php">
						<div class="col-xs-6 col-md-3">
							<div class="panel panel-orange hvr-float-shadow">
								<div class="panel-heading dark-overlay">Usuarios</div>
								<div class="panel-body">
									<p>Ingrese aquí para administrar los usuarios.</p>
								</div>
							</div>
						</div>
					</a>
					
					<a href="impOrdenes.php">
						<div class="col-xs-6 col-md-2">
							<div class="panel panel-red hvr-grow-rotate">
								<div class="panel-heading dark-overlay">Órdenes</div>
								<div class="panel-body">
									<p>Ingrese para imprimir órdenes</p>
								</div>
							</div>
						</div>
					</a>
			</div><!--/.row-->






		</div><!--/.container-->
				
	</div>	<!--/.main-->





	<!-------------------------------------------------------------------------------->

	
	<script src="bootstrap/js/jquery-1.11.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/chart.min.js"></script>
	<script src="bootstrap/js/chart-data.js"></script>
	<script src="bootstrap/js/easypiechart.js"></script>
	<script src="bootstrap/js/easypiechart-data.js"></script>
	<script src="bootstrap/js/bootstrap-datepicker.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="js/materialize.min.js"></script>
</body>

</html>
<?php
    }  
    else {
        header ("Location: index.php");
    }
?>
