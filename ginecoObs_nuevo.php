<?php
    session_start();
    include ("conexion.php");
    if(isset($_SESSION['usuario_nombre'])){  
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Valentin</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="bootstrap/css/datepicker3.css" rel="stylesheet">
	<link href="bootstrap/css/styles.css" rel="stylesheet">	

</head>
<body>
	<?php include('navbar2.php'); ?>
	<?php include('sidebar2.php'); ?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="home.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li><a href="pacientes.php">Pacientes</a></li>
				<li class="active">Nuevo paciente</li>
			</ol>
		</div><!--/.row-->
		<br />
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2 style="color:rgb(48, 165, 255)"><img src="img/icons/1447882687_Add-Male-User.png"> Paciente nuevo - Antecedentes Gineco-Obstetricos</h2>
				</div>

				<div class="col-md-11">
					<div class="panel panel-default">
						<div class="panel-body tabs">
							<div class="tab-content formularios">
								<form action="ginecoObs_guarda.php" method="POST">
								<div class="row"><br>
									<div class="col-md-4 form-inline">
										<div class="form-group">
										    <label for="exampleInputName2"><strong class="text-forms-pacientes">Menarca: </strong></label>
										    <input type="text" class="form-control" id="exampleInputName2" placeholder=" " name="menarca_pac">
									  	</div>
									</div>
									<div class="col-md-4 form-inline">
										<div class="form-group">
										    <label for="exampleInputName2"><strong class="text-forms-pacientes">RM: </strong></label>
										    <input type="text" class="form-control" id="exampleInputName2" placeholder=" " name="rm_pac">
									  	</div>
									</div>
									<div class="col-md-4 form-inline">
										<div class="form-group">
										    <label for="exampleInputName2"><strong class="text-forms-pacientes">Menopausia: </strong></label>
										    <input type="text" class="form-control" id="exampleInputName2" placeholder=" " name="menopausia_pac">
									  	</div>
									</div>
								</div><br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">Gestas: </strong></label>
											<input class="form-control" type="text" name="gestas_pac" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">Partos: </strong></label>
											<input class="form-control" type="text" name="partos_pac" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">CS: </strong></label>
											<input class="form-control" type="text" name="cs_pac" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">AB: </strong></label>
											<input class="form-control" type="text" name="ab_pac" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">EE: </strong></label>
											<input class="form-control" type="text" name="ee_pac" placeholder=""/>
									  	</div>
									</div>
								</div><br>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">FUM: </strong></label>
											<input class="form-control" type="date" name="fum_pac" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">Dismenorrea (si / no): </strong></label>
											<input class="form-control" type="text" name="dismenorrea_pac" placeholder=""/>
									  	</div>
									</div>
								</div><br>
								<div class="row">
								<div class="col-md-2">
									<label>
										<strong class="text-forms-pacientes">Anticoncepción: </strong>
									</label>
								</div>
									<div class="col-md-4">
										<div class="form-group">
											<div class="col-md-6">
												<input class="form-control" type="text" name="tipo_anticoncep_pac" placeholder="Tipo"/>
											</div>
											<div class="col-md-6">
												<input class="form-control" type="text" name="tiempo_anticoncep_pac" placeholder="Tiempo"/>
											</div>
									  	</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<textarea class="form-control" rows="3" name="notas_anticoncep_pac" placeholder="NOTAS"></textarea>
										</div>
									</div>
								</div><br><!--Cierra row-->
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label><strong class="text-forms-pacientes">Cirugías Ginecológicas: </strong></label>
											<textarea class="form-control" rows="3" name="cirug_ginec_pac" placeholder="Ej: 21/10/2015 ----> Parto natural."></textarea>
										</div>
									</div>
								</div><br>
								<!-- Id Paciente -->
										<input type="hidden" class="form-control" id="idPaciente" placeholder="" name="id_paciente" value= "<?php echo $_REQUEST["id"]; ?>" >
								<div class="row">
									<div class="col-md-3">
										<input type="submit" class="btn btn-success btn-lg btn-block hoverable" name="guardar" value="Guardar"/>
									</div>
									<div class="col-md-9"></div>
								</div>
							</form>
							</div>
						</div>
					</div><!--/.panel-->
				</div><!--/.col-->
			</div><!--cierra row-->
		</div><!--cierra container-->
	</div>

	<script src="bootstrap/js/jquery-1.11.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/bootstrap-table.js"></script>
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="js/materialize.min.js"></script>
</body>
</htm>
<?php
    }  
    else {
        header ("Location: index.php");
    }
?>