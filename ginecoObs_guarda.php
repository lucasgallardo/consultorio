<?php
    session_start();
    include ("conexion.php");
    if(isset($_SESSION['usuario_nombre'])){  
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Valentin</title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/datepicker3.css" rel="stylesheet">
        <link href="bootstrap/css/styles.css" rel="stylesheet">	

    </head>
    <?php include('navbar2.php'); ?>
	<?php include('sidebar2.php'); ?>
    <body class="body-background">
        

        <?php
        if (isset($_POST['guardar']))
        {

            $menarca_pac = mysqli_real_escape_string($conexion, $_POST['menarca_pac']);
            $rm_pac = mysqli_real_escape_string($conexion, $_POST['rm_pac']);
            $menopausia_pac = mysqli_real_escape_string($conexion, $_POST['menopausia_pac']);
            $gestas_pac = mysqli_real_escape_string($conexion, $_POST['gestas_pac']);
            $partos_pac = mysqli_real_escape_string($conexion, $_POST['partos_pac']);
            $cs_pac = mysqli_real_escape_string($conexion, $_POST['cs_pac']);
            $ab_pac = mysqli_real_escape_string($conexion, $_POST['ab_pac']);
            $ee_pac = mysqli_real_escape_string($conexion, $_POST['ee_pac']);
            $fum_pac = mysqli_real_escape_string($conexion, $_POST['fum_pac']);
            $dismenorrea_pac = mysqli_real_escape_string($conexion, $_POST['dismenorrea_pac']);
            $tipo_anticoncep_pac = mysqli_real_escape_string($conexion, $_POST['tipo_anticoncep_pac']);
            $tiempo_anticoncep_pac = mysqli_real_escape_string($conexion, $_POST['tiempo_anticoncep_pac']);
            $notas_anticoncep_pac = mysqli_real_escape_string($conexion, $_POST['notas_anticoncep_pac']);
            $cirug_ginec_pac = mysqli_real_escape_string($conexion, $_POST['cirug_ginec_pac']);
            $id_paciente = mysqli_real_escape_string($conexion, $_POST['id_paciente']);

            $reg_paciente = mysqli_query($conexion, "INSERT INTO antecedentes_ginecoObs(menarca_pac,rm_pac,menopausia_pac,gestas_pac,partos_pac,cs_pac,ab_pac,ee_pac,fum_pac,dismenorrea_pac,tipo_anticoncep_pac,tiempo_anticoncep_pac,notas_anticoncep_pac,cirug_ginec_pac,idPaciente_ginecoObs) VALUES ('$menarca_pac','$rm_pac','$menopausia_pac','$gestas_pac','$partos_pac','$cs_pac','$ab_pac','$ee_pac','$fum_pac','$dismenorrea_pac','$tipo_anticoncep_pac','$tiempo_anticoncep_pac','$notas_anticoncep_pac','$cirug_ginec_pac','$id_paciente')") or die(mysqli_error($conexion));

            if ($reg_paciente)
            {
                //header("Location: examen_fisico_nuevo.php?id=$id_paciente");
                ?>
                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
                    <div class="container">
                       <hr>
                        <div class="row">
                            <div class="col-md-11">
                                <div class="container-fluid">
                                    <div class="text-center"
                                     <div class="col-xs-6 col-md-3">
                                        <div class="panel panel-teal hoverable">
                                            <div class="panel panel-heading dark-overlay">
                                                <h3><span style="color:white">Datos guardados correctamente!!</span></h3>
                                            </div>
                                            <div class="panel-body">
                                                <a href="detallesPaciente.php?id=<?php echo "$id_paciente"; ?>"><button class="btn btn-info btn-lg"><span class="glyphicon glyphicon-eye-open"></span> Ver detalles de paciente</button></a>
                                                <a href="pacientes.php"><button class="btn btn-info btn-lg"><span class="glyphicon glyphicon-list"></span> Ver listado de paciente</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--Cierra el main-->
                
                    <?php
                }
                else
                {
                    echo "error";
                }
            }
            ?>
    </body>
</html>
<?php
    }  
    else {
        header ("Location: index.php");
    }
?>