<?php include('conexion.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Valentin</title>
	<link rel="stylesheet" href="css/materialize.min.css">
	<link rel="stylesheet" href="css/index.css">
	<!--
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      -->
</head>
<body class="body-background">

<!--Barra de navegación-->
<nav>
    <div class="nav-wrapper #039be5 light-blue darken-1"><!--le cambio el color al nav -->
      <a href="index.php" class="brand-logo">&nbsp Consultorio Valentin</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="index.php"><i class="material-icons left">home</i>Inicio</a></li>
        <li><a href="ayuda.php"><i class="material-icons left">info</i>Ayuda</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="index.php"><i class="material-icons left">home</i>Inicio</a></li>
        <li><a href="ayuda.php"><i class="material-icons left">info</i>Ayuda</a></li>
      </ul>
    </div>
</nav><!--Fin barra de navegación-->

<?php 
	if (isset($_POST['enviar'])) {
		$user_nombre = mysqli_real_escape_string($conexion,$_POST['user']);
		$user_email = mysqli_real_escape_string($conexion,$_POST['email']);
		$user_contra=mysqli_real_escape_string($conexion,$_POST['contra']);
		$user_tipo=mysqli_real_escape_string($conexion,$_POST['tipo']);
		$sql = mysqli_query($conexion, "SELECT usuario_nombre from usuarios where usuario_nombre='$user_nombre'");
		if (mysqli_num_rows($sql)>0) {
			echo "El nombre de usuario elegido ya existe";
		}
		else{
			$reg = mysqli_query($conexion, "INSERT INTO usuarios(usuario_nombre,usuario_email,usuario_contra,tipo_user) VALUES ('$user_nombre','$user_email','$user_contra','$user_tipo')") or die(mysqli_error($conexion));
				if ($reg) {
					//echo "Datos ingresados correctamente";
					?>
					<!--aca remplazamos el echo con un alert-->
					<div class="container">
						<div clas="row">
							<div class="col s12 m12 l12">
								<div class="card-panel #76ff03 light-green accent-3 hoverable">
									<p>
										<span class="white-text">
			    							<i class="material-icons small">done</i> &nbsp Bien hecho! Usuario creado correctamente.
			    						</span>
									</p>
								</div>
							</div>
						</div>
					</div>
					<meta http-equiv="Refresh" content="2; url=index.php">
					<!--acontinuación abrimos de nuevo pehp para cerrar el corchete del if-->
				<?php
				}
				else{
					echo "Error al guardar los datos";
					//poner un div y reemplazar el echo-----MAS ADELANTE-----
				}
		}
	}
?>
<br>
<div class="container">

    <div class="row">
			<div class="col s12 m12 "><!--grid 6 al centro-->
				<div class="card-panel z-depth-4">
					<div class="orange-text">
						<span class="center">
							<h4><strong>Bienvenido<strong></h4>
				        </span>
				        <p class="center">Por favor complete todos los campos para continuar</p>
			        </div>
			        <br>

			        <form action="" method="POST" >
						<div class="row">
							<div class="input-field">
							<i class="material-icons prefix">account_circle</i>
							<input id="icon_prefix" type="text" class="validate" name="user" required="">
							<label for="icon_prefix">Usuario:</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field">
							<i class="material-icons prefix">email</i>
							<input id="email" name="email" type="email" class="validate" required="">
							<label for="email" data-error="Por favor ingrese un dato válido *" data-success="Correcto *">Email</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field">
								<i class="material-icons prefix">lock</i>
								<input id="icon_prefix" type="password" class="validate" name="contra" required="">
								<label for="icon_prefix">Password</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field">
							<i class="material-icons prefix">description</i>
								<select multiple name="tipo">
									<option value="" disabled selected>¿Qué tipo de usuario eres?</option>
									<option>Doctora</option>
									<option>Secretaria</option>
									<option>Administrador</option>
								</select>
								<label>Tipo de usuario</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m6 center">
								<input class="btn waves-effect waves-light light-blue hoverable" type="submit" name="enviar" value="Registrarse">
								</input>
							</div>
							<div class="col s12 m6 center">
								<a href="index.php" class="btn waves-effect waves-light red hoverable">Volver <i class="material-icons left">undo</i></a>
							</div>
						</div><!--cierro botones-->
			        </form>
	        	</div>
			</div>
	</div><!--aca termina el row-->

<br><br>
<hr>
<br>
<blockquote>
	Página creada por Pablo Valles.
</blockquote>
</div><!--Cierra el container-->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/materialize.min.js"></script>
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
  })
</script>
<script>
	  $(document).ready(function() {
    $('select').material_select();
  });
</script>
</body>
</html>