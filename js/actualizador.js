$(function () {
    $("#guardaExamen").click(function () {
        var url = "examenObstetricoAjax.php";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#formularioExamen").serialize(),
            success: function (data)
            {
                $("#resultadoExamen").html(data);
                $("#formularioExamen").clearQueue();
            }
        });
        return false;
    });
});