<?php
session_start();
include ("../conexion.php");
date_default_timezone_set('America/Argentina');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
<style>
    p{
        font-size: 12px;
    }
    .estudios{
        list-style-type: circle;
        font-size: 12px;
    }
    .titulos{
        font-size: 14px;
        text-align: center;
    }
</style>
</head>
<body>

	<?php
		$fechaDeHoy = date("d/m/y h:m");			
		$nombre = "Analítica1.doc";
		
		header('Content-type: application/vnd.ms-word');
		header("Content-Disposition: attachment; filename=$nombre");
		header("Pragma: no-cache");
		header("Expires: 0");
	?>
<?php
$recibir = $_REQUEST['accion'];

$ver = mysqli_query($conexion, "SELECT * from pacientes where id_paciente='$recibir'");
if ($reg = mysqli_fetch_array($ver)){
    
?>
<div class="titulos">
   DRA. FABIANA VALLES <br>
   MAT. 1997 <br>
   GINECOLOGIA-OBSTETRICIA
</div>
<hr>
<p>Apellido: <?php echo $reg['apellido_pac'] ?> y Nombre: <?php echo $reg['nombre_pac'] ?></p>
<p>Obra Social: <?php echo $reg['obsocial_pac'] ?></p>
<p>Número de obra Social: <?php echo $reg['nro_obsocial_pac'] ?></p>
<p>Plan Obra social: <?php echo $reg['plan_obsocial_pac'] ?></p>
<ul class="estudios">
    <li>Hemograma</li>
    <li>Recuento de plaquetas</li>
    <li>Fibrinógeno</li>
    <li>Glucemia</li>
    <li>Uremia</li>
    <li>Uricemia</li>
    <li>Creatininemia</li>
    <li>T.P. - TTPK</li>
    <li>TSH - T4 libre</li>
    <li>GOT</li>
    <li>GPT</li>
    <li>FAL</li>
    <li>LDH</li>
    <li>BILIRRUBINA (D-I-T)</li>
    <li>Orina Completa</li>
    <li>Grupo Sanguíneo </li>
    <li>Factor Rh</li>
    <li>VDRL</li>
    <li>HIV</li>
    <li>HBsAg</li>
    <li>Chagas</li>
    <li>IgG  antiTOXOPLASMOSIS</li>
    <li>IgG  anti RUBEOLA</li>
    <li>IgG  antiCMV</li>
</ul>
<p><u>DIAG.:</u></p>
<br><br>
<hr>
<p style="text-align:center; width:30px">
CDTE FOSSA N° 98  ESQ.SERU  (B°BOMBAL) CIUDAD					
SAN MARTIN N° 1440 - UNIDAD 2 -GODOY CRUZ						
TELEF.0261-4246059   Y     0261-4245182						
TELEF.MOVIL 154-605712						
</p> 
<?php }
?>
</body>
</html>