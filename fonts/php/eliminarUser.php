<?php
    session_start();
    include ("../conexion.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Valentin</title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bootstrap/css/datepicker3.css" rel="stylesheet">
    <link href="../bootstrap/css/bootstrap-table.css" rel="stylesheet">
    <link href="../bootstrap/css/styles.css" rel="stylesheet">
    <link rel="stylesheet" href="../bootstrap/css/hover.css"/>
    <link href="../css/index.css">
    <script src="../bootstrap/js/lumino.glyphs.js"></script>
</head>
<body>
<?php 
include ("../navbar2.php");

?>
<?php $id = $_REQUEST['accion'];?>
<div>
<hr>
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <div class="panel panel-default" style="border:4px solid #ccc">
                <div class="panel-heading dark-overlay ">¿Seguro Desea eliminar el usuario?</div>
                <div class="panel-body">
                    <form action="" method="POST" class="form-inline" role="form">
                            <input type="hidden" value="<?php echo $id;?>" name="id_us">
                            <div style="text-align:center">
                            <input name="borrar" type="submit" class="btn btn-danger btn-lg hvr-wobble-horizontal" value="SI, ELIMINAR">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="../usuarios.php" class="btn btn-info btn-lg hvr-bounce-to-top2">NO, CANCELAR</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
if (isset($_POST['borrar'])){
    $id_us = mysqli_real_escape_string($conexion,$_POST['id_us']);
    $sql = mysqli_query($conexion, "delete from usuarios where id_usuario = '$id_us'");
    ?>
    <div class="alert alert-danger">
        <img src="../img/icons/1447530030_high_priority.png" alt=""><strong>Eliminado correctamente</strong> será reedirigido a Usuarios...
        <meta http-equiv="Refresh" content="2; url=../usuarios.php">
    </div>
    <?php
}    
?>

</div>
</body>
</html>