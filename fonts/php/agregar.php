<?php
session_start();
include ("../conexion.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Valentin</title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bootstrap/css/datepicker3.css" rel="stylesheet">
    <link href="../bootstrap/css/bootstrap-table.css" rel="stylesheet">
    <link href="../bootstrap/css/styles.css" rel="stylesheet">
    <link href="../css/index.css">
    <script src="../bootstrap/js/lumino.glyphs.js"></script>
</head>
<body>
<?php 
include ("../navbar2.php");
include ("../sidebar2.php");
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
<hr>

<?php    
if(!empty($_POST)){
    if (isset($_POST['submit'])) {

      $usuario_nombre = mysqli_real_escape_string($conexion,$_POST['name']);
      $usuario_email = mysqli_real_escape_string($conexion,$_POST['mail']);
      $usuario_clave = mysqli_real_escape_string($conexion,$_POST['pass']);
      $tipo = mysqli_real_escape_string($conexion,$_POST['tipo']);

             //comprueba que el usuario no esté repetido
      $sql = mysqli_query($conexion, "SELECT usuario_nombre FROM usuarios WHERE usuario_nombre='$usuario_nombre'");
      if (mysqli_num_rows($sql)>0) {    
                ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Error</strong> El Usuario agregado ya existe...!
                    <meta http-equiv="Refresh" content="2; url=../usuarios.php">
                </div>
              <?php  
      } else {
          //$usuario_clave = crypt($usuario_clave,"pass"); //contraseña encriptada con crypt
          $reg = mysqli_query($conexion, "INSERT INTO usuarios(usuario_nombre,usuario_email,usuario_contra,tipo_user) VALUES ('$usuario_nombre','$usuario_email','$usuario_clave','$tipo')") or die(mysqli_error($reg));
          if ($reg) {
              ?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Bien hecho!!</strong> Usuario agregado correctamente...!
                    <meta http-equiv="Refresh" content="2; url=../usuarios.php">
                </div>
              <?php
          } else {
                ?>
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Error</strong> en la consulta..!
                    <meta http-equiv="Refresh" content="2; url=../usuarios.php">
                </div>
              <?php  
          } 
        }
      } 
    }
?>

</div>
</body>
</html>