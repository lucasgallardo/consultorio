<?php
    session_start();
    include ("../conexion.php");
    if (isset($_POST['s'])){
    
    $busca = mysqli_real_escape_string($conexion,$_POST['s']);
    } 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Valentin</title>

        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/datepicker3.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap-table.css" rel="stylesheet">
        <link href="bootstrap/css/styles.css" rel="stylesheet">

        <style>
            
                ::selection{
                    background-color: rgba(28, 235, 20, 0.93);
                    color: white;
                    text-shadow: 1px 1px black;
                }

        </style>
        <!--Icons-->
        <script src="bootstrap/js/lumino.glyphs.js"></script>

        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
     <?php include('navbar2.php'); ?>
        <?php include('sidebar2.php'); ?>


        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main "> <!--MAS IMPORTANTE-->		
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="home.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                    <li class="active">Ordenes a imprimir</li>
                </ol>
            </div><!--/.row-->
            <hr>
            <div class="container">
                <br>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel panel-teal">
                                <div class="panel-heading dark-overlay">
                                    <h2 style="color:white">DRA. FABIANA VALLES</h2>
                                </div>
                                
                            <div class="panel-body">
                                   <p style="text-align:center">MAT. 1997</p>
                                    <h5 style="text-align:center; color:white">GINECOLOGIA-OBSTETRICIA</h5>
                                    <div class="col-md-12">
                                   	<form method="post" class="navbar-form navbar-right" role="search" action="">
                                      <div class="form-group">
                                        <input type="text" name="s" class="form-control" placeholder="Buscar">
                                      </div>
                                      <button type="submit" class="btn btn-success" ><i class="glyphicon glyphicon-search"></i> </button>
                                      
                                    </form>                                         
                                </div>
                                    <?php
                                    if(empty($busca)){
                                         echo "no has filtrado por paciente";
                                    }                   
                                    else{
                                        $ask= mysqli_query($conexion,"select * from pacientes where apellido_pac like '%".$busca."%' or dni_pac like '%".$busca."%'");
                                    }
                                        
                                    while ($traer = mysqli_fetch_array($ask)) {
                                        $id = $traer['id_paciente'];
                                        ?>
                                        <form action="" method="POST" role="form">
                                        <legend style="color:white">Orden de Consulta</legend>
                                        <div class="form-group">
                                            <label for="">Apellido</label>
                                            <input type="text" class="form-control" id="" value="<?php print $traer['apellido_pac']?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Nombres</label>
                                            <input type="text" class="form-control" id="" value="<?php print $traer['nombre_pac'] ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Obra Social</label>
                                            <input type="text" class="form-control" id="" value="<?php print $traer['obsocial_pac'] ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="">N° Obra Social</label>
                                            <input type="text" class="form-control" id="" value="<?php print $traer['nro_obsocial_pac'] ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="">DNI</label>
                                            <input type="text" class="form-control" id="" value="<?php print $traer['dni_pac'] ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Edad</label>
                                            <input type="text" class="form-control" id="" value="<?php print $traer['edad_pac'] ?>">
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="">Domicilio</label>
                                            <input type="text" class="form-control" id="" value="<?php print $traer['direc_pac'] ?>">
                                        </div>
                                        <br>
                                </form>
                                        <?php echo "<a href='php/imprimirOrdenConsulta.php?accion=$id'><button class='btn btn-warning btn-lg hoverable'><span class='glyphicon glyphicon-print'></span> Imprimir</button></a>";?>
                                        <?php
                                    }
                                    ?>
                            <br><br><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>.....................................................</p>
                                    <p>Afiliado</p>                        
                                </div>
                                <div class="col-md-6">
                                    <p>.....................................................</p>
                                    <p>Prestador</p>                        
                                </div>
                            </div>                      
                            </div>
                            <div class="panel-footer panel-teal">
                                <h5 style="text-align:center; color:white">
                                CDTE FOSSA N° 98  ESQ.SERU  (B°BOMBAL) CIUDAD					
                                SAN MARTIN N° 1440 - UNIDAD 2 -GODOY CRUZ						
                                 TELEF.0261-4246059   Y     0261-4245182						
                                TELEF.MOVIL 154-605712						
                                </h5>
                                 
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div><!--/.row-->	

            </div><!--/.container-->

        </div>	<!--/.main-->





        <!-------------------------------------------------------------------------------->


        <script src="bootstrap/js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap-table.js"></script>
        <script src="js/jquery-2.2.3.min.js"></script>
        <script src="js/materialize.min.js"></script>
    </body>

</html>

