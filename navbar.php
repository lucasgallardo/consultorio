<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Valentin</title>
	<link rel="stylesheet" href="css/materialize.min.css">
	<link rel="stylesheet" href="css/index.css">
	<!--
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      -->
</head>
<body>
<!--Barra de navegación-->
<nav>
<ul id="dropdown1" class="dropdown-content">
<li><a href="#">Editar</a></li>
<li><a href="#">Info</a></li>
<li class="divider"></li>
<li><a href="#">Salir</a></li>
</ul>
    <div class="nav-wrapper #039be5 light-blue darken-1"><!--le cambio el color al nav -->
      <a href="#!" class="brand-logo">&nbsp Consultorio</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="index.php"><i class="material-icons left">home</i>Inicio</a></li>
        <li><a href="#"><i class="material-icons left">perm_identity</i>Pacientes</a></li>
        <li><a href="#"><i class="material-icons left">description</i>Estudios</a></li>
        <li><a href="#"><i class="material-icons left">settings</i>Configuración</a></li>
        <li><a class="dropdown-button" href="#" data-activates="dropdown1">Mi usuario<i class="material-icons right">arrow_drop_down</i></a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="index.php"><i class="material-icons left">home</i>Inicio</a></li>
        <li><a href="#"><i class="material-icons left">perm_identity</i>Pacientes</a></li>
        <li><a href="#"><i class="material-icons left">description</i>Estudios</a></li>
        <li><a href="#"><i class="material-icons left">settings</i>Configuración</a></li>
        <li><a class="dropdown-button" href="#" data-activates="dropdown1">Mi usuario<i class="material-icons right">arrow_drop_down</i></a></li>
      </ul>
    </div>

</nav><!--Fin barra de navegación-->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/materialize.min.js"></script>
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
  })
</script>
<script>
    $(document).ready(function() {
    $('select').material_select();
  });
</script>
<script>
  $(document).ready(function()block{
    $(".dropdown-button").dropdown();
  })
</script>