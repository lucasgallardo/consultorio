<?php
    session_start();
    include ("conexion.php");

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Valentin</title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/datepicker3.css" rel="stylesheet">
        <link href="bootstrap/css/styles.css" rel="stylesheet">	

    </head>
    <body>
        <?php include('navbar2.php'); ?>
        <?php include('sidebar2.php'); ?>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="home.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                    <li><a href="pacientes.php">Pacientes</a></li>
                    <li class="active">Nuevo paciente</li>
                </ol>
            </div><!--/.row-->
            <br />
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 style="color:rgb(48, 165, 255)"><img src="img/icons/1447882687_Add-Male-User.png"> Paciente nuevo - Examen físico</h2>
                    </div>

                    <div class="col-md-11">
                        <div class="panel panel-default">
                            <div class="panel-body tabs">
                                <div class="tab-content" style="background-color:#e9ecf2">
                                    <form action="guarda_examen_fisico.php" method="POST">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">Fecha: </strong></label>
                                                    <input class="form-control" type="date" name="fecha_ex_fisico_pac">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">MC: </strong></label>
                                                    <input class="form-control" type="tex" name="mc_pac">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row"><br>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">Examen Físico: </strong></label>
                                                    <input class="form-control" type="tex" name="ex_fisico_pac">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <legend></legend>
                                            <div class="col-md-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="colposcopia" class="control-label col-xs-2"> Colposcopia: </label>
                                                        <div class="col-xs-8">
                                                            <input id="colposcopia" type="text" class="form-control" placeholder="" name="colpos_pac">
                                                        </div>
                                                        <a href="" class="btn btn-warning btn-md">?</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="pap" class="control-label col-xs-2"> Pap: </label>
                                                        <div class="col-xs-8">
                                                            <input id="pap" type="text" class="form-control" placeholder="" name="pap_pac">
                                                        </div>
                                                        <a href="" class="btn btn-success btn-md">?</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-md-2">Ecografía Ginecológica: </label>
                                                        <div class="col-xs-8">
                                                            <input id="" type="text" class="form-control" placeholder="" name="eco_ginec_pac">
                                                        </div>
                                                        <a href="" class="btn btn-primary btn-md">?</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-xs-2">Ecografía Mamaria: </label>
                                                        <div class="col-xs-8">
                                                            <input id="" type="text" class="form-control" placeholder="" name="eco_mam_pac">
                                                        </div>
                                                        <a href="" class="btn btn-info btn-md">?</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-xs-2">Mamografía: </label>
                                                        <div class="col-xs-8">
                                                            <input id="" type="text" class="form-control" placeholder="" name="mamograf_pac">
                                                        </div>
                                                        <a href="" class="btn btn-danger btn-md">?</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <legend></legend>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">Otros: </strong></label>
                                                    <textarea class="form-control" rows="5" name="otros_ex_fis_pac" placeholder=""></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">Notas: </strong></label>
                                                    <textarea class="form-control" rows="5" name="notas_ex_fis_pac" placeholder=""></textarea>
                                                </div>
                                            </div>
                                        </div><br>
                                        <!-- Id Paciente -->
                                        <input type="hidden" class="form-control" id="idPaciente" placeholder="" name="id_paciente" value= "<?php echo $_REQUEST["id"]; ?>" >
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="submit" class="btn btn-info btn-lg btn-block hoverable" name="guardar" value="Guardar"/>
                                            </div>
                                            <div class="col-md-9"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!--/.panel-->
                    </div><!--/.col-->
                </div><!--cierra row-->
            </div><!--cierra container-->
        </div>

        <script src="bootstrap/js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap-table.js"></script>
        <script src="js/jquery-2.2.3.min.js"></script>
        <script src="js/materialize.min.js"></script>
    </body>
</htm>
