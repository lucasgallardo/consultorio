-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-06-2016 a las 01:57:18
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `valentin`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antecedentes_ginecoobs`
--

CREATE TABLE `antecedentes_ginecoobs` (
  `id_ginecoObs` int(11) NOT NULL,
  `menarca_pac` varchar(45) DEFAULT NULL,
  `rm_pac` varchar(45) DEFAULT NULL,
  `menopausia_pac` varchar(45) DEFAULT NULL,
  `gestas_pac` varchar(45) DEFAULT NULL,
  `partos_pac` varchar(45) DEFAULT NULL,
  `cs_pac` varchar(45) DEFAULT NULL,
  `ab_pac` varchar(45) DEFAULT NULL,
  `ee_pac` varchar(45) DEFAULT NULL,
  `fum_pac` varchar(45) DEFAULT NULL,
  `dismenorrea_pac` varchar(45) DEFAULT NULL,
  `tipo_anticoncep_pac` varchar(45) DEFAULT NULL,
  `tiempo_anticoncep_pac` varchar(45) DEFAULT NULL,
  `notas_anticoncep_pac` varchar(45) DEFAULT NULL,
  `cirug_ginec_pac` varchar(45) DEFAULT NULL,
  `idPaciente_ginecoObs` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `antecedentes_ginecoobs`
--

INSERT INTO `antecedentes_ginecoobs` (`id_ginecoObs`, `menarca_pac`, `rm_pac`, `menopausia_pac`, `gestas_pac`, `partos_pac`, `cs_pac`, `ab_pac`, `ee_pac`, `fum_pac`, `dismenorrea_pac`, `tipo_anticoncep_pac`, `tiempo_anticoncep_pac`, `notas_anticoncep_pac`, `cirug_ginec_pac`, `idPaciente_ginecoObs`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 2),
(2, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(3, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(4, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(5, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(6, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(7, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(8, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(9, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(10, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(11, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(12, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(13, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(14, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(15, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(16, 'rrttp', 'ssa88129', '1231', '1', '1', '0', '0', '0', '1212-12-13', 'no', 'dns', '40 dias', 'sjdksajj jksadnksdas', 'mjsadka', 3),
(17, '12', '2', '12', '12', '12', '1', '12', '12', '2221-12-13', 'si', 'nos', 'q', 'sd', 'ejemplo', 4),
(18, '12', '2', '12', '12', '12', '1', '12', '12', '2221-12-13', 'si', 'nos', 'q', 'sd', 'ejemplo', 4),
(19, '', '', '', '', '', '', '', '', '', '', '', '', 'sdas', 'sadasd', 5),
(20, '13', '123', '43', '1', '1', '1', '0', '0', '2121-12-13', 'pol', 'dre', 'sad', 'sadasdsadasdasdasdasda', 'jsakdalkjsadj klmkdnasd', 6),
(21, '100', '12', 'no', '4', '3', '1', '1', '0', '2015-10-29', 'no', 'ifco', '60dias', 'kikikikilolololo', 'tantannsakkk kkk kk kk k kk kk kk kk', 7),
(22, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 11),
(23, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 12),
(24, '12', '3/28', 'no', '1', '1', '', '', '', '2016-06-15', 'no', 'aco', '5 aÃ±os', 'yasmin divina', '', 13),
(25, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1),
(26, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 2),
(27, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 3),
(28, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 4),
(29, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 5),
(30, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 6),
(31, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 7),
(32, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 8),
(33, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antecedentes_personales`
--

CREATE TABLE `antecedentes_personales` (
  `id_antecedente` int(200) NOT NULL,
  `diabetes` varchar(45) DEFAULT NULL,
  `hipertension` varchar(45) DEFAULT NULL,
  `hipotiroidismo` varchar(45) DEFAULT NULL,
  `tabaquismo` varchar(45) DEFAULT NULL,
  `peso` varchar(45) DEFAULT NULL,
  `altura` varchar(45) DEFAULT NULL,
  `imc` varchar(45) DEFAULT NULL,
  `antecedentes_alergico` varchar(45) DEFAULT NULL,
  `antecedentes_clinicos` varchar(45) DEFAULT NULL,
  `antecedentes_quirurgicos` varchar(45) DEFAULT NULL,
  `farmacos` varchar(45) DEFAULT NULL,
  `madre` varchar(45) DEFAULT NULL,
  `padre` varchar(45) DEFAULT NULL,
  `otros_antecedentes` varchar(45) DEFAULT NULL,
  `notas_antecedentes` varchar(45) DEFAULT NULL,
  `idPaciente_personales` int(11) DEFAULT NULL,
  `antecedentes_personalescol` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `antecedentes_personales`
--

INSERT INTO `antecedentes_personales` (`id_antecedente`, `diabetes`, `hipertension`, `hipotiroidismo`, `tabaquismo`, `peso`, `altura`, `imc`, `antecedentes_alergico`, `antecedentes_clinicos`, `antecedentes_quirurgicos`, `farmacos`, `madre`, `padre`, `otros_antecedentes`, `notas_antecedentes`, `idPaciente_personales`, `antecedentes_personalescol`) VALUES
(1, 'on', 'on', '', '', '12312', '123123', '123123', '213123', '123123', '123123', '213123', '213123qsdasda', 'asdas', 'das', 'dasdasd', 2, NULL),
(2, '', 'on', 'on', '', '49', '20', '100', 'palop', 'sadkanslk ', 'sakdjasldj', 'sdjasdkas', 'N', 'n', 'n', 'samjdkahsfk \r\nsadjawjsk\r\nsadskj sd\r\nasadasd', 3, NULL),
(3, 'on', 'on', 'on', 'on', '100', '100', '100', 'ejemplo', 'ejemplo', 'ejemplo', 'ejemplo', 'ejemplo', 'ejemplo', 'ejemplo', 'ejemplo', 4, NULL),
(4, 'on', 'on', '', '', '212', '1231', '12312', '12312', '213123', '1231', 'assdww', '213', '213', '2131', '123123', 5, NULL),
(5, 'on', 'on', 'on', 'on', '80kg', '1.80', '887', 'odiajsdioj ASNDJGHUHAS ', 'SAIDJASIODASJAWS', 'JSADKAJSNDKAJSN JK', 'NSADLKADJKASJDLASJ', 'hipertension', 'xxx', 'resticiia', 'kjsdklajdasdjsl', 6, NULL),
(6, 'on', 'on', 'on', 'on', '99', '1.58', '78,9', 'jsjadhjdhajksdh', 'xx', 'xx', 'xx', 'hipoglusemia', 'nada', 'kaka', 'lioliolioliolio\r\n', 7, NULL),
(9, 'on', 'on', 'on', 'on', '50', '1.60', '19.531249999999996', 'no', 'no', 'si cesaria', 'levo', 'nada', 'nada', 'nada', '', 13, NULL),
(10, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, NULL),
(11, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 2, NULL),
(12, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 3, NULL),
(13, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 4, NULL),
(14, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 5, NULL),
(15, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 6, NULL),
(16, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 7, NULL),
(17, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 8, NULL),
(18, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 9, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_fisico`
--

CREATE TABLE `examen_fisico` (
  `id_examen_fisico` int(11) NOT NULL,
  `fecha_ex_fisico_pac` date DEFAULT NULL,
  `mc_pac` varchar(45) DEFAULT NULL,
  `ex_fisico_pac` varchar(45) DEFAULT NULL,
  `colpos_pac` varchar(45) DEFAULT NULL,
  `pap_pac` varchar(45) DEFAULT NULL,
  `eco_ginec_pac` varchar(45) DEFAULT NULL,
  `eco_mam_pac` varchar(45) DEFAULT NULL,
  `mamograf_pac` varchar(45) DEFAULT NULL,
  `otros_ex_fis_pac` varchar(45) DEFAULT NULL,
  `notas_ex_fis_pac` varchar(45) DEFAULT NULL,
  `idPaciente_ex_fisico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_obstetrico`
--

CREATE TABLE `examen_obstetrico` (
  `id_examen_obstetrico` int(11) NOT NULL,
  `fecha_examen_obstetrico` date DEFAULT NULL,
  `eg_examen_obstetrico` varchar(45) DEFAULT NULL,
  `pa_examen_obstetrico` varchar(45) DEFAULT NULL,
  `au_examen_obstetrico` varchar(45) DEFAULT NULL,
  `ta_examen_obstetrico` varchar(45) DEFAULT NULL,
  `lcf_examen_obstetrico` varchar(45) DEFAULT NULL,
  `edema_examen_obstetrico` varchar(45) DEFAULT NULL,
  `datos_examen_obstetrico` varchar(45) DEFAULT NULL,
  `id_paciente_examen_obstetrico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `examen_obstetrico`
--

INSERT INTO `examen_obstetrico` (`id_examen_obstetrico`, `fecha_examen_obstetrico`, `eg_examen_obstetrico`, `pa_examen_obstetrico`, `au_examen_obstetrico`, `ta_examen_obstetrico`, `lcf_examen_obstetrico`, `edema_examen_obstetrico`, `datos_examen_obstetrico`, `id_paciente_examen_obstetrico`) VALUES
(3, '2016-06-07', '12', '2', '123', '12', '123', '123', '123', 0),
(4, '0000-00-00', '', '', '', '', '', '', '', 0),
(5, '2016-06-01', '12312', '21312', '123123', '21312', '123', '123', '123', 0),
(6, '0000-00-00', '', '', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `id_paciente` int(250) NOT NULL,
  `apellido_pac` varchar(200) DEFAULT NULL,
  `nombre_pac` varchar(200) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `edad_pac` varchar(50) DEFAULT NULL,
  `dni_pac` varchar(50) DEFAULT NULL,
  `tel_paciente` varchar(100) DEFAULT NULL,
  `profesion_pac` varchar(100) DEFAULT NULL,
  `empresa_pac` varchar(100) DEFAULT NULL,
  `obsocial_pac` varchar(300) DEFAULT NULL,
  `plan_obsocial_pac` varchar(200) DEFAULT NULL,
  `nro_obsocial_pac` varchar(50) DEFAULT NULL,
  `estado_civil_pac` varchar(100) DEFAULT NULL,
  `direc_pac` varchar(200) DEFAULT NULL,
  `contacto_pac` varchar(200) DEFAULT NULL,
  `mail` varchar(200) DEFAULT NULL,
  `gru_sanguineo_pac` varchar(100) DEFAULT NULL,
  `fecha_primera_consulta` date DEFAULT NULL,
  `notas` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`id_paciente`, `apellido_pac`, `nombre_pac`, `fecha_nacimiento`, `edad_pac`, `dni_pac`, `tel_paciente`, `profesion_pac`, `empresa_pac`, `obsocial_pac`, `plan_obsocial_pac`, `nro_obsocial_pac`, `estado_civil_pac`, `direc_pac`, `contacto_pac`, `mail`, `gru_sanguineo_pac`, `fecha_primera_consulta`, `notas`) VALUES
(1, 'AIROLDI', 'ANA LAURA', '1982-02-05', '34 aÃ±os', ' 29.326.524 ', '155139035', '', '', 'GALENO', 'PLATA', '017625620103', 'Casada', 'COLON 723 CIUDAD -CP: 5500', '', 'airoldial@hotmail.com', '', '2016-06-20', 'Ver nueva base de datos'),
(2, 'PAGANI', 'PAULA E.', '1978-10-17', '37 aÃ±os', ' 26.812.930 ', '4314231', '', '', 'OMINT', 'GENESIS', '4263727800013', 'Casada', 'VALDIVIA	811 DORREGO 5519', '', '', '', '2016-06-20', 'actualizar con nueva base de datos'),
(3, 'DEGARBO', 'JULIANA', '1984-06-28', '32 aÃ±os', ' 30.924.372 ', '155021720', '', '', 'OSDE', '210 NO GRAVADO', '61720424001', 'Casada', 'BÂº LOS OLIVOS MC C18 LUJAN DE CUYO 5507', '', '', '', '2016-06-20', 'ACTUALIZAR'),
(4, 'DIAMANTE', 'URSULA AMIRA', '1986-07-28', '29 aÃ±os', ' 32.316.387 ', '154606997', '', '', 'OSDE', '210 NO GRAVADO', '61922031603', 'Casada', 'PEDRO J. GODOY	980	GODOY CRUZ	5501', '', '', '', '2016-06-20', 'ACTUALIZAR'),
(5, 'GUTIERREZ', 'MARIA FERNANDA', '1982-06-12', '34 aÃ±os', ' 29.373.402 ', '155875323', '', '', 'OSEP', '3126', '29373402/00', 'Casada', 'BÂº DOLORES PRATS DE HUISI M40 C40 GODOY CRUZ 5501', '', '', '', '2016-06-20', 'ACTUALIZAR'),
(6, 'MAZZONI', 'GABRIELA', '1980-03-24', '36 aÃ±os', ' 27.765.810 ', '153649727 / 4375488', '', '', 'CIMESA', '', '13-2765810/00', 'Casada', 'ESTRADA 237	LAS HERAS 5539', '', 'gmazzoni1980@yahoo.com.ar', '', '2016-06-20', 'ACTUALIZAR'),
(7, 'ONOFRI', 'PAULA', '0000-00-00', 'NaN aÃ±os', ' 26.615.713 ', '154189102', '', '', 'OSPE', 'A-421', '20-28598282-5/01', 'Casada', 'MARTIN RODRIGUEZ 325 DORREGO 5519', '', '', '', '2016-06-20', 'actualizar'),
(8, 'RUBIO', 'VALERIA MARIA', '1981-08-04', '34 aÃ±os', ' 28.926.844 ', '155790765', '', '', 'OSDE', '210 NO GRAVADO', '61968705201', 'Casada', 'DIAZ VELEZ 1953 DORREGO 5519', '', '', '', '2016-06-20', 'actualizar'),
(9, 'SANCHEZ', 'ELISABETH', '1953-07-24', '62 aÃ±os', ' 10.514.825 ', '4318076', '', '', 'OSEP', '', '10514825/00', 'Casada', 'HUARPES 868	CIUDAD CP:5500', '', '', '', '2016-06-20', 'actualizar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(200) NOT NULL,
  `usuario_nombre` varchar(200) NOT NULL,
  `usuario_email` varchar(200) NOT NULL,
  `usuario_contra` varchar(200) NOT NULL,
  `tipo_user` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario_nombre`, `usuario_email`, `usuario_contra`, `tipo_user`) VALUES
(1, 'Pablo', 'p@h', '1234', ''),
(14, 'Fabiana', 'fabiana@fabi', '1234', ''),
(15, 'lucas', 'lucas@mail.com', '123', ''),
(16, 'Marita', 'marita@biblioteca.com', '1234', 'Secretaria');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `antecedentes_ginecoobs`
--
ALTER TABLE `antecedentes_ginecoobs`
  ADD PRIMARY KEY (`id_ginecoObs`);

--
-- Indices de la tabla `antecedentes_personales`
--
ALTER TABLE `antecedentes_personales`
  ADD PRIMARY KEY (`id_antecedente`);

--
-- Indices de la tabla `examen_fisico`
--
ALTER TABLE `examen_fisico`
  ADD PRIMARY KEY (`id_examen_fisico`);

--
-- Indices de la tabla `examen_obstetrico`
--
ALTER TABLE `examen_obstetrico`
  ADD PRIMARY KEY (`id_examen_obstetrico`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`id_paciente`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `antecedentes_ginecoobs`
--
ALTER TABLE `antecedentes_ginecoobs`
  MODIFY `id_ginecoObs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `antecedentes_personales`
--
ALTER TABLE `antecedentes_personales`
  MODIFY `id_antecedente` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `examen_fisico`
--
ALTER TABLE `examen_fisico`
  MODIFY `id_examen_fisico` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `examen_obstetrico`
--
ALTER TABLE `examen_obstetrico`
  MODIFY `id_examen_obstetrico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `id_paciente` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
