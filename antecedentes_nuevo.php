<?php
    session_start();
    include ("conexion.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Valentin</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="bootstrap/css/datepicker3.css" rel="stylesheet">
	<link href="bootstrap/css/styles.css" rel="stylesheet">	

</head>
<body>
	<?php include('navbar2.php'); ?>
	<?php include('sidebar2.php'); ?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="home.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li><a href="pacientes.php">Pacientes</a></li>
				<li class="active">Nuevo paciente</li>
			</ol>
		</div><!--/.row-->
		<br />
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2 style="color:rgb(48, 165, 255)"><img src="img/icons/1447882687_Add-Male-User.png"> Paciente nuevo - Antecedentes</h2>
				</div>

				<div class="col-md-11">
					<div class="panel panel-default">
						<div class="panel-body tabs">
							<div class="tab-content formularios">
<script>
function obtenerSuma()
{
document.getElementById('resultado').value =
parseFloat(document.getElementById('dividendo1').value) /
Math.pow(parseFloat(document.getElementById('dividendo2').value),2);
}
</script> 
								<form action="guarda_antecedentes.php" method="POST">
									<div class="row"><br>
										<div class="col-md-6">
											<div class="checkbox">
											    <label>
											      	<input name="ch_pac_diab" type="checkbox"> <strong class="text-forms-pacientes">Diabetes </strong>
											    </label>
										  	</div>
										  	<div class="checkbox">
											    <label>
											      	<input name="ch_pac_hiper" type="checkbox"> <strong class="text-forms-pacientes">Hipertensión </strong>
											    </label>
										  	</div>
										  	<div class="checkbox">
											    <label>
											      	<input name="ch_pac_hipo" type="checkbox"> <strong class="text-forms-pacientes">Hipotiroidismo </strong>
											    </label>
										  	</div>
										  	<div class="checkbox">
											    <label>
											      	<input name="ch_pac_tabaq" type="checkbox"> <strong class="text-forms-pacientes">Tabaquismo </strong>
											    </label>
										  	</div>
										</div>
										<div class="col-md-6">
											<div class="col-md-6">
												<label><strong class="text-forms-pacientes">Peso: </strong></label>
												<input onkeyup="obtenerSuma();" id="dividendo1" class="form-control" type="text" name="peso_pac" placeholder=""/>
											</div>
											<div class="col-md-6">
												<label><strong class="text-forms-pacientes">Altura: </strong></label>
												<input onkeyup="obtenerSuma();" id="dividendo2" class="form-control" type="text" name="altura_pac" placeholder=""/>
												<br>
											</div>
											
											<div class="col-md-12">
											 	<label><strong class="text-forms-pacientes">IMC: </strong></label>
												<input id="resultado" class="form-control" type="text" name="imc_pac" placeholder="" value=""/>
								            </div>
										</div>
									</div><br><!--Cierro row-->
									<div class="row">
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Antecedentes Alérgicos: </strong></label>
											<textarea class="form-control" rows="4" name="ant_alergicos_pac"></textarea>
										</div>
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Antecedentes Clínicos: </strong></label>
											<textarea class="form-control" rows="4" name="ant_clinicos_pac"></textarea>
										</div>
									</div><br><!--Cierro row-->
									<div class="row">
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Antecedentes Quirúrgicos: </strong></label>
											<textarea class="form-control" rows="4" name="ant_quir_pac"></textarea>
										</div>
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Fármacos: </strong></label>
											<textarea class="form-control" rows="4" name="farmacos_pac"></textarea>
										</div>
									</div><br>
									<div class="row">	
										<div class="col-md-6">
										<label><strong class="text-forms-pacientes">Antecedentes Heredo-Familiares: </strong></label><br>
											<div class="form-group">
												<label class="sr-only" ></label>
												<div class="input-group">
													<div class="input-group-addon">Madre: </div>
													<input type="text" class="form-control" id="exampleInputAmount" placeholder="" name="madre_pac">
												</div>
											</div>
											<div class="form-group">
												<label class="sr-only" ></label>
												<div class="input-group">
													<div class="input-group-addon">Padre: </div>
													<input type="text" class="form-control" id="exampleInputAmount" placeholder="" name="padre_pac">
												</div>
											</div>
											<div class="form-group">
												<label class="sr-only" ></label>
												<div class="input-group">
													<div class="input-group-addon">Otros: </div>
													<input type="text" class="form-control" id="exampleInputAmount" placeholder="" name="otros_pac">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Notas: </strong></label>
											<br>
											<textarea class="form-control" rows="6" name="notas_heredo_fam_pac"></textarea>
										</div>
										<!-- Id Paciente -->
										<input type="hidden" class="form-control" id="idPaciente" placeholder="" name="id_paciente" value= "<?php echo $_REQUEST["id"]; ?>" >
								
									</div><!--Cierro row--><br>
									<div class="row">
										<div class="col-md-3">
										<input type="submit" class="btn btn-warning btn-lg btn-block hoverable" name="guardar" value="Guardar"/>
									</div>
									<div class="col-md-9"></div>
									</div><br><!--Cierro row-->
								</form>
							</div>
						</div>
					</div><!--/.panel-->
				</div><!--/.col-->
			</div><!--cierra row-->
		</div><!--cierra container-->
	</div>

	<script src="bootstrap/js/jquery-1.11.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/bootstrap-table.js"></script>
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="js/materialize.min.js"></script>
</body>
</htm>

