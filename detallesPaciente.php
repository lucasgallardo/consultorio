<?php
session_start();
include ("conexion.php");
include ("operaciones.php");
if (isset($_SESSION['usuario_nombre']))
{
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Detalles Paciente</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
            <link href="bootstrap/css/datepicker3.css" rel="stylesheet">
            <link href="bootstrap/css/styles.css" rel="stylesheet">

            <!--Icons-->
            <script src="bootstrap/js/lumino.glyphs.js"></script>

            <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
            <![endif]-->
        </head>
        <body>
            <?php
            $id = $_REQUEST[id];
            $paciente = armaPaciente($conexion, $id);
            print_r($paciente);
            ?>
            <?php include ('navbar2.php') ?>
            <?php include ('sidebar2.php') ?>
            <?php
            ?>
        </body>
    </html>
    <?php
}
else
{
    header("Location: index.php");
}
?>
