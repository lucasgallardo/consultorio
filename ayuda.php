<?php include('conexion.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Valentin</title>
	<link rel="stylesheet" href="css/materialize.min.css">
	<link rel="stylesheet" href="css/index.css">
	<!--
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      -->
</head>
<body class="body-background">
<nav>
    <div class="nav-wrapper #039be5 light-blue darken-1"><!--le cambio el color al nav -->
      <a href="index.php" class="brand-logo">&nbsp&nbsp Consultorio Valentin</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="index.php"><i class="material-icons left ">home</i>Inicio</a></li>
        <li><a href="ayuda.php"><i class="material-icons left">info</i>Ayuda</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="index.php"><i class="material-icons left orange-text">home</i>Inicio</a></li>
        <li><a href="#"><i class="material-icons left light-blue-text">info</i>Ayuda</a></li>
      </ul>
    </div>
</nav><!--Fin barra de navegación-->
<br/>
<div class="container">
	<div class="card-panel ">
	<h4 style="color:#ff6f00">Bienvenido.<br></h4> A continuación unas recomendaciones para el manejo del sistema<br />
	<hr><br />
		 <ul class="collapsible popout" data-collapsible="accordion">
			<li>
				<div class="collapsible-header hoverable orange-text">
					<i class="material-icons ">done_all</i>Ingreso de usuario
				</div>
				<div class="collapsible-body">
					<p>1) Dirigirse al boton de registro</p>
					<div class="container"><img class="materialboxed" width="250" src="img/ayuda/ing_usuarios_1.png"></div>
					<p>2) Completar los datos<br>
					3) Apretar el boton celeste (registrarse)</p>
					<div class="container"><img class="materialboxed" width="250" src="img/ayuda/ing_usuarios_2.png"></div><br>
				</div>
			</li>
			<li>
				<div class="collapsible-header hoverable light-green-text">
				<i class="material-icons ">assignment_ind</i>Carga de pacientes</div>
				<div class="collapsible-body"><p>
					
				</p></div>
			</li>
			<li>
				<div class="collapsible-header light-blue-text hoverable"><i class="material-icons">whatshot</i>Third</div>
				<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
			</li>
		</ul>
	</div>

	<br><br>
</div>




</div><!--Cierra el container-->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/materialize.min.js"></script>
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
  })
</script>
<script>
	  $(document).ready(function() {
    $('select').material_select();
  });
</script>
<script>
	$(document).ready(function(){
    $('.collapsible').collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
  });
</script>
</body>
</html>