<?php

function armaPaciente($conexion, $id) {
    $buscaPaciente = mysqli_query($conexion, "SELECT * FROM
            pacientes,
            antecedentes_personales,
            antecedentes_ginecoObs
            WHERE
            pacientes.id_paciente='$id'
            AND antecedentes_ginecoObs.idPaciente_ginecoObs='$id'
            AND antecedentes_personales.idPaciente_personales='$id' ") or die(mysqli_error($conexion));
    $resultado = mysqli_fetch_array($buscaPaciente);
    $buscaExamenfisico = mysqli_query($conexion, "SELECT * FROM examen_fisico WHERE idPaciente_ex_fisico='$id' ") or die(mysqli_error($conexion));
    $resultado2 = mysqli_fetch_array($buscaExamenfisico);
    $buscaFichaObs = mysqli_query($conexion, "SELECT * FROM examen_obstetrico WHERE id_paciente_examen_obstetrico='$id' ") or die(mysqli_error($conexion));
    $resultado3 = mysqli_fetch_array($buscaFichaObs);

    return array(
        //datos personales paciente
        'id_paciente' => $resultado["id_paciente"],
        'apellido_paciente' => $resultado["apellido_pac"],
        'nombre_paciente' => $resultado["nombre_pac"],
        'fecha_nacimiento' => $resultado["fecha_nacimiento"],
        'edad_paciente' => $resultado["edad_pac"],
        'dni_paciente' => $resultado["dni_pac"],
        'telefono_paciente' => $resultado["tel_pac"],
        'profesion_paciente' => $resultado["profesion_pac"],
        'empresa_paciente' => $resultado["empresa_pac"],
        'obra_social_paciente' => $resultado["obsocial_pac"],
        'plan_obra_social_paciente' => $resultado["plan_obsocial_pac"],
        'numero_obra_social_paciente' => $resultado["nro_obsocial_pac"],
        'estado_civil_paciente' => $resultado["estado_civil_pac"],
        'direccion_paciente' => $resultado["direc_pac"],
        'contacto_paciente' => $resultado["contacto_pac"],
        'mail_paciente' => $resultado["mail"],
        'grupo_sanguineo_paciente' => $resultado["gru_sanguineo_pac"],
        'fecha_primera_consulta_paciente' => $resultado["fecha_primera_consulta"],
        'notas' => $resultado["notas"],
        'fecha_examen_fisico' => $resultado2["fecha_ex_fisico_pac"],
        'mc_pac' => $resultado2["mc_pac"],
        'examen_fisico' => $resultado2["ex_fisico_pac"],
        'otros_examen_fisico' => $resultado2["otros_ex_fis_pac"],
        'notas_examen_fisico' => $resultado2["notas_ex_fis_pac"]
            //antecedentes personales
            //antecedentes gineco-obstetricos
            //examen fisico
            //ficha obstetrica
    );
}

function cargaColposcopia($conexion, $id) {
    $buscaColposcopia = mysqli_query($conexion, "SELECT * FROM colposcopia WHERE id_paciente_colposcopia='$id' ORDER BY fecha_colposcopia DESC");
    if (mysqli_num_rows($buscaColposcopia) > 0) {
        while ($row = mysqli_fetch_array($buscaColposcopia)) {
            echo "<table class='table table-hover table-bordered table-condensed'>";
            echo "<tbody>";
            echo "<tr>";
            echo "<td width='9%'>" . $row['fecha_colposcopia'] . "</td>";
            echo "<td width='9%'>" . $row['datos_colposcopia'] . "</td>";
            echo "<td width='4%'></td>";
            echo "</tr>";
            echo "<tbody>";
            echo "</table>";
        }
    }
}
?>

