<?php include('conexion.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Valentin</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/css/datepicker3.css" rel="stylesheet">
<link href="bootstrap/css/styles.css" rel="stylesheet">	

</head>
<body>
	<?php include('navbar2.php') ?>
	<?php include('sidebar2.php') ?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="home.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li><a href="pacientes.php">Pacientes</a></li>
			<li class="active">Nuevo paciente</li>
		</ol>
	</div><!--/.row-->
	<br />
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 style="color:rgb(48, 165, 255)"><img src="img/icons/1447882687_Add-Male-User.png"> Paciente nuevo</h2>

			</div>
			
			<div class="col-md-11">
				<div class="panel panel-default">
					<div class="panel-body tabs">
						<ul class="nav nav-tabs nav-justified">
							<li class="active"><a href="#tab1" data-toggle="tab">Datos Personales</a></li>
							<li><a href="#tab2" data-toggle="tab">Antecedentes personales</a></li>
							<li><a href="#tab3" data-toggle="tab">Antec. Gineco-obstetricos</a></li>
							<li><a href="#tab4" data-toggle="tab">Examen físico</a></li>
							<li><a href="#tab5" data-toggle="tab">Ficha obstetrica</a></li>
						</ul>
		
						<div class="tab-content" style="background-color:#e9ecf2">
							<div class="tab-pane fade in active" id="tab1">
								<form action="func_pacienteNuevo.php" method="POST">
									<div class="row">
										<div class="form-group">
											<div class="col-md-6">
												<label><strong class="text-forms-pacientes">Apellido: </strong></label><input class="form-control" type="text" name="apellido_paciente" placeholder="Apellido paciente"/>
											</div>

											<div class="col-md-6">
												<label><strong class="text-forms-pacientes">Nombre: </strong></label><input class="form-control" type="text" name="nombre_paciente" placeholder="Nombre paciente"/>
											</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Fecha de nacimiento: </strong></label><input class="form-control" type="date" name="nacimiento_pac"/>
										</div>
										<div class="col-md-2">
											<label><strong class="text-forms-pacientes">Edad: </strong></label><input class="form-control" type="text" name="edad_paciente" placeholder="Ingrese la edad"/>
										</div>
										<div class="col-md-3">
											<label><strong class="text-forms-pacientes">DNI: </strong></label>
											<input class="form-control" type="text" name="dni_paciente" placeholder="DNI"/>
										</div>
										<div class="col-md-3">
											<label><strong class="text-forms-pacientes">Teléfono: </strong></label>
											<input class="form-control" type="text" name="tel_paciente" placeholder=""/>
										</div>
									</div>
									<br/>	
									<div class="row">
										<div class="col-md-5">
											<label><strong class="text-forms-pacientes">Profesión: </strong></label>
											<input class="form-control" type="text" name="prof_paciente" placeholder="Ej... Abogada"/>
										</div>
										<div class="col-md-1">
											
										</div>
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Empresa: </strong></label>
											<input class="form-control" type="text" name="empresa_paciente" placeholder="..."/>
										</div>
									</div><br/>
									<div class="row">
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Obra Social: </strong></label>
											<input class="form-control" type="text" name="osocial_paciente" placeholder="..."/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Plan: </strong></label>
											<input class="form-control" type="text" name="plan_osocial_pac" placeholder="..."/>
										</div>
										<div class="col-md-3">
											<label><strong class="text-forms-pacientes">Número de Obra social: </strong></label>
											<input class="form-control" type="text" name="nro_osocial_pac" placeholder="11111"/>
										</div>
										<div class="col-md-1"><label><strong class="text-forms-pacientes">Ayuda</strong></label><br>
											<a class="btn btn-info btn-sm" href="" role="button">?</a>
										</div>
									</div><br>
									<div class="row">
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Dirección: </strong></label>
											<input class="form-control" type="text" name="dire_paciente" placeholder=""/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Contacto: </strong></label>
											<input class="form-control" type="text" name="contacto_paciente" placeholder=""/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">E-mail: </strong></label>
											<input class="form-control" type="text" name="email_paciente" placeholder="ejemplo@hotmail.com"/>
										</div>
									</div><br>
									<div class="row">
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Grupo sanguineo: </strong></label>
											<input class="form-control" type="text" name="GS_paciente" placeholder=""/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Fecha de 1ra consulta: </strong></label>
											<input class="form-control" type="date" name="fecha_1_consulta_paciente" placeholder="..."/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Estado civil: </strong></label>
											<select class="form-control" name="estado_civil_paciente">
												<option>Casada</option>
												<option>Divorciada</option>
												<option>Viuda</option>
												<option>Concubinato</option>
												<option>Soltera</option>
											</select>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12">
											<label><strong class="text-forms-pacientes">Motivo de consulta: </strong></label>
											<textarea class="form-control" rows="3" name="motivo_paciente"></textarea>
										</div>
									</div><br>
									<div class="row">
										<div class="col-md-12">
											<label><strong class="text-forms-pacientes">Notas adicionales</strong></label>
											<textarea class="form-control has success" rows="3" name="notas_adicionales_paciente"></textarea>
										</div>
									</div><br>
									<div class="row">
										<div class="col-md-12">
											<input name="guardar" type="submit" class="btn btn-primary btn-lg hoverable" value="Guardar"/>
										</div>
									</div>

								</form>
							</div>
<!--***********************************************************************************************-->
							<div class="tab-pane fade" id="tab2">
								<form action="func_pacienteNuevo.php" method="POST">
									<div class="row">
										<div class="col-md-6">
											<input class="form-control" type="text" name="id_paciente" value=""/>
										</div>
										<div class="col-md-6">
											
										</div>
									</div>
									<div class="row"><br>
										<div class="col-md-6">
											<div class="checkbox">
											    <label>
											      	<input name="ch_pac_diab" type="checkbox"> <strong class="text-forms-pacientes">Diabetes </strong>
											    </label>
										  	</div>
										  	<div class="checkbox">
											    <label>
											      	<input name="ch_pac_hiper" type="checkbox"> <strong class="text-forms-pacientes">Hipertensión </strong>
											    </label>
										  	</div>
										  	<div class="checkbox">
											    <label>
											      	<input name="ch_pac_hipo" type="checkbox"> <strong class="text-forms-pacientes">Hipotiroidismo </strong>
											    </label>
										  	</div>
										  	<div class="checkbox">
											    <label>
											      	<input name="ch_pac_tabaq" type="checkbox"> <strong class="text-forms-pacientes">Tabaquismo </strong>
											    </label>
										  	</div>
										</div>
										<div class="col-md-6">
											<div class="col-md-6">
												<label><strong class="text-forms-pacientes">Peso: </strong></label>
												<input class="form-control" type="text" name="peso_pac" placeholder=""/>
											</div>
											<div class="col-md-6">
												<label><strong class="text-forms-pacientes">Altura: </strong></label>
												<input class="form-control" type="text" name="altura_pac" placeholder=""/>
												<br>
											</div>
											<div class="col-md-12">
												<label><strong class="text-forms-pacientes">IMC: </strong></label>
												<input class="form-control" type="text" name="imc_pac" placeholder=""/>
											</div>
										</div>
									</div><br><!--Cierro row-->
									<div class="row">
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Antecedentes Alérgicos: </strong></label>
											<textarea class="form-control" rows="4" name="ant_alergicos_pac"></textarea>
										</div>
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Antecedentes Clínicos: </strong></label>
											<textarea class="form-control" rows="4" name="ant_clinicos_pac"></textarea>
										</div>
									</div><br><!--Cierro row-->
									<div class="row">
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Antecedentes Quirúrgicos: </strong></label>
											<textarea class="form-control" rows="4" name="ant_quir_pac"></textarea>
										</div>
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Fármacos: </strong></label>
											<textarea class="form-control" rows="4" name="farmacos_pac"></textarea>
										</div>
									</div><br>
									<div class="row">	
										<div class="col-md-6">
										<label><strong class="text-forms-pacientes">Antecedentes Heredo-Familiares: </strong></label><br>
											<div class="form-group">
												<label class="sr-only" ></label>
												<div class="input-group">
													<div class="input-group-addon">Madre: </div>
													<input type="text" class="form-control" id="exampleInputAmount" placeholder="" name="madre_pac">
												</div>
											</div>
											<div class="form-group">
												<label class="sr-only" ></label>
												<div class="input-group">
													<div class="input-group-addon">Padre: </div>
													<input type="text" class="form-control" id="exampleInputAmount" placeholder="" name="padre_pac">
												</div>
											</div>
											<div class="form-group">
												<label class="sr-only" ></label>
												<div class="input-group">
													<div class="input-group-addon">Otros: </div>
													<input type="text" class="form-control" id="exampleInputAmount" placeholder="" name="otros_pac">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Notas: </strong></label>
											<br>
											<textarea class="form-control" rows="6" name="notas_heredo_fam_pac"></textarea>
										</div>
								
									</div><!--Cierro row--><br>
									<div class="row">
										<div class="col-md-3">
										<input type="submit" class="btn btn-warning btn-lg btn-block hoverable" name="guardar2" value="Guardar"/>
									</div>
									<div class="col-md-9"></div>
									</div><br><!--Cierro row-->
								</form>
							</div>
<!--***********************************************************************************************-->
							<div class="tab-pane fade" id="tab3">
							<form action="" method="POST">
								<div class="row"><br>
									<div class="col-md-4 form-inline">
										<div class="form-group">
										    <label for="exampleInputName2"><strong class="text-forms-pacientes">Menarca: </strong></label>
										    <input type="text" class="form-control" id="exampleInputName2" placeholder=" " name="menarca_pac">
									  	</div>
									</div>
									<div class="col-md-4 form-inline">
										<div class="form-group">
										    <label for="exampleInputName2"><strong class="text-forms-pacientes">RM: </strong></label>
										    <input type="text" class="form-control" id="exampleInputName2" placeholder=" " name="rm_pac">
									  	</div>
									</div>
									<div class="col-md-4 form-inline">
										<div class="form-group">
										    <label for="exampleInputName2"><strong class="text-forms-pacientes">Menopausia: </strong></label>
										    <input type="text" class="form-control" id="exampleInputName2" placeholder=" " name="menopausia_pac">
									  	</div>
									</div>
								</div><br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">Gestas: </strong></label>
											<input class="form-control" type="text" name="gestas_pac" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">Partos: </strong></label>
											<input class="form-control" type="text" name="partos_pac" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">CS: </strong></label>
											<input class="form-control" type="text" name="cs_pac" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">AB: </strong></label>
											<input class="form-control" type="text" name="ab_pac" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">EE: </strong></label>
											<input class="form-control" type="text" name="ee_pac" placeholder=""/>
									  	</div>
									</div>
								</div><br>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">FUM: </strong></label>
											<input class="form-control" type="date" name="" placeholder=""/>
									  	</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										    <label><strong class="text-forms-pacientes">Dismenorrea (si / no): </strong></label>
											<input class="form-control" type="text" name="dismenorrea_pac" placeholder=""/>
									  	</div>
									</div>
								</div><br>
								<div class="row">
								<div class="col-md-2">
									<label>
										<strong class="text-forms-pacientes">Anticoncepción: </strong>
									</label>
								</div>
									<div class="col-md-4">
										<div class="form-group">
											<div class="col-md-6">
												<input class="form-control" type="text" name="tipo_anticoncep_pac" placeholder="Tipo"/>
											</div>
											<div class="col-md-6">
												<input class="form-control" type="text" name="tiempo_anticoncep_pac" placeholder="Tiempo"/>
											</div>
									  	</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<textarea class="form-control" rows="3" name="notas_anticoncep_pac" placeholder="NOTAS"></textarea>
										</div>
									</div>
								</div><br><!--Cierra row-->
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label><strong class="text-forms-pacientes">Cirugías Ginecológicas: </strong></label>
											<textarea class="form-control" rows="3" name="cirug_ginec_pac" placeholder="Ej: 21/10/2015 ----> Parto natural."></textarea>
										</div>
									</div>
								</div><br>
								<div class="row">
									<div class="col-md-3">
										<input type="submit" class="btn btn-success btn-lg btn-block hoverable" name="guardar3" value="Guardar"/>
									</div>
									<div class="col-md-9"></div>
								</div>
							</form>
							</div>
<!--********************************************************************************************-->
							<div class="tab-pane fade" id="tab4">
								
							</div>
<!--******************************************************************************************-->
							<div class="tab-pane fade" id="tab5">
								
							</div>
<!--******************************************************************************************-->
						</div>
					</div>
				</div><!--/.panel-->
			</div><!--/.col-->
		</div><!--cierra row-->
	</div><!--cierra container-->
</div>

	<script src="bootstrap/js/jquery-1.11.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/bootstrap-table.js"></script>
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="js/materialize.min.js"></script>
</body>
</htm>