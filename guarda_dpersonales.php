<?php 
include('conexion.php');
?><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Valentin</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/css/datepicker3.css" rel="stylesheet">
<link href="bootstrap/css/styles.css" rel="stylesheet">	

</head>
<body class="body-background">

<?php

if (isset($_POST['guardar'])) {
	
	$apellido_paciente = mysqli_real_escape_string ($conexion, $_POST['apellido_paciente']);
	$nombre_paciente = mysqli_real_escape_string($conexion, $_POST['nombre_paciente']);
	$fecha_nacimiento_paciente = mysqli_real_escape_string($conexion, $_POST['nacimiento_pac']);
	$edad_paciente = mysqli_real_escape_string($conexion, $_POST['edad_paciente']);
	$dni_paciente = mysqli_real_escape_string($conexion, $_POST['dni_paciente']);
	$tel_paciente = mysqli_real_escape_string($conexion, $_POST['tel_paciente']);
	$prof_paciente = mysqli_real_escape_string($conexion, $_POST['prof_paciente']);
	$empresa_paciente = mysqli_real_escape_string($conexion, $_POST['empresa_paciente']);
	$osocial_paciente = mysqli_real_escape_string($conexion, $_POST['osocial_paciente']);
	$plan_osocial_paciente = mysqli_real_escape_string($conexion, $_POST['plan_osocial_pac']);
	$nro_osocial_paciente = mysqli_real_escape_string($conexion, $_POST['nro_osocial_pac']);
	$dire_paciente = mysqli_real_escape_string($conexion, $_POST['dire_paciente']);
	$contacto_paciente = mysqli_real_escape_string($conexion, $_POST['contacto_paciente']);
	$email_paciente = mysqli_real_escape_string($conexion, $_POST['email_paciente']);
	$gs_paciente = mysqli_real_escape_string($conexion, $_POST['GS_paciente']);
	$fecha_1_consulta_paciente = mysqli_real_escape_string($conexion, $_POST['fecha_1_consulta_paciente']);
	$motivo_consulta_paciente = mysqli_real_escape_string($conexion, $_POST['motivo_paciente']);
	$notas= mysqli_real_escape_string($conexion, $_POST['notas_adicionales_paciente']);
	$estado_civil_pac = mysqli_real_escape_string($conexion, $_POST['estado_civil_paciente']);

	$sql=mysqli_query($conexion, "SELECT dni_pac from pacientes where dni_pac='$dni_paciente'");

	if (mysqli_num_rows($sql) > 0) {
		echo "Error en DNI. El mismo ya ha sido registrado a otro paciente.";
	}else{
		$reg_paciente = mysqli_query($conexion, "INSERT INTO pacientes(apellido_pac,nombre_pac,fecha_nacimiento,edad_pac,dni_pac,tel_paciente,profesion_pac,empresa_pac,obsocial_pac,plan_obsocial_pac,nro_obsocial_pac,direc_pac,contacto_pac,mail,gru_sanguineo_pac,fecha_primera_consulta,motivo_consulta,notas,estado_civil_pac) VALUES ('$apellido_paciente','$nombre_paciente','$fecha_nacimiento_paciente','$edad_paciente','$dni_paciente','$tel_paciente','$prof_paciente','$empresa_paciente','$osocial_paciente','$plan_osocial_paciente','$nro_osocial_paciente','$dire_paciente','$contacto_paciente','$email_paciente','$gs_paciente','$fecha_1_consulta_paciente','$motivo_consulta_paciente','$notas','$estado_civil_pac')") or die(mysqli_error($conexion));
		
		if ($reg_paciente) {			
			$idPaciente = mysqli_query($conexion, "SELECT id_paciente from pacientes where dni_pac='$dni_paciente' limit 1") or die(mysqli_error($conexion));
			while ($ej = mysqli_fetch_array($idPaciente)) {
				$id=$ej['id_paciente'];
				header("Location: antecedentes_nuevo.php?id=$id");
			}
		}else{
			echo "error";
		}
	}
}

?>
</body>
</html>