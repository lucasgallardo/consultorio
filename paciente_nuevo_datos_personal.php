<?php
    session_start();
    include ("conexion.php");
    if(isset($_SESSION['usuario_nombre'])){  
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Valentin</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="bootstrap/css/datepicker3.css" rel="stylesheet">
	<link href="bootstrap/css/styles.css" rel="stylesheet">	

</head>
<body>
	<?php include('navbar2.php') ?>
	<?php include('sidebar2.php') ?>

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="home.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li><a href="pacientes.php">Pacientes</a></li>
				<li class="active">Nuevo paciente</li>
			</ol>
		</div><!--/.row-->
		<br />
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2 style="color:rgb(48, 165, 255)"><img src="img/icons/1447882687_Add-Male-User.png"> Paciente nuevo Datos Personales</h2>
				</div>
                <script src="bootstrap/js/calcularEdad.js"></script>
				<div class="col-md-11">
					<div class="panel panel-default">
						<div class="panel-body tabs">
							<div class="tab-content formularios">
								<form action="guarda_dpersonales.php" method="POST">
									<div class="row">
										<div class="form-group">
											<div class="col-md-6">
												<label><strong class="text-forms-pacientes">Apellido: </strong></label><input class="form-control" type="text" name="apellido_paciente" placeholder="Apellido paciente"/>
											</div>

											<div class="col-md-6">
												<label><strong class="text-forms-pacientes">Nombre: </strong></label><input class="form-control" type="text" name="nombre_paciente" placeholder="Nombre paciente"/>
											</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Fecha de nacimiento: </strong></label><input class="form-control" type="date" name="nacimiento_pac" onblur="edad(this.value)"/>
										</div>
										<div class="col-md-2">
											<label><strong class="text-forms-pacientes">Edad: </strong></label><input class="form-control" type="text" name="edad_paciente" id="result"  placeholder="Ingrese la edad" id="user_date"/>
										</div>
										<div class="col-md-3">
											<label><strong class="text-forms-pacientes">DNI: </strong></label>
											<input class="form-control" type="text" name="dni_paciente" placeholder="DNI"/>
										</div>
										<div class="col-md-3">
											<label><strong class="text-forms-pacientes">Teléfono: </strong></label>
											<input class="form-control" type="text" name="tel_paciente" placeholder=""/>
										</div>
									</div>
									<br/>	
									<div class="row">
										<div class="col-md-5">
											<label><strong class="text-forms-pacientes">Profesión: </strong></label>
											<input class="form-control" type="text" name="prof_paciente" placeholder="Ej... Abogada"/>
										</div>
										<div class="col-md-1">
											
										</div>
										<div class="col-md-6">
											<label><strong class="text-forms-pacientes">Empresa: </strong></label>
											<input class="form-control" type="text" name="empresa_paciente" placeholder="..."/>
										</div>
									</div><br/>
									<div class="row">
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Obra Social: </strong></label>
											<input class="form-control" type="text" name="osocial_paciente" placeholder="..."/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Plan: </strong></label>
											<input class="form-control" type="text" name="plan_osocial_pac" placeholder="..."/>
										</div>
										<div class="col-md-3">
											<label><strong class="text-forms-pacientes">Número de Obra social: </strong></label>
											<input class="form-control" type="text" name="nro_osocial_pac" placeholder="11111"/>
										</div>
									</div><br>
									<div class="row">
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Dirección: </strong></label>
											<input class="form-control" type="text" name="dire_paciente" placeholder=""/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Contacto: </strong></label>
											<input class="form-control" type="text" name="contacto_paciente" placeholder=""/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">E-mail: </strong></label>
											<input class="form-control" type="text" name="email_paciente" placeholder="ejemplo@hotmail.com"/>
										</div>
									</div><br>
									<div class="row">
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Grupo sanguineo: </strong></label>
											<input class="form-control" type="text" name="GS_paciente" placeholder=""/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Fecha de 1ra consulta: </strong></label>
											<input class="form-control" type="date" name="fecha_1_consulta_paciente" value="<?php echo date("Y-m-d");?>"/>
										</div>
										<div class="col-md-4">
											<label><strong class="text-forms-pacientes">Estado civil: </strong></label>
											<select class="form-control" name="estado_civil_paciente">
												<option>Casada</option>
												<option>Divorciada</option>
												<option>Viuda</option>
												<option>Concubinato</option>
												<option>Soltera</option>
											</select>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12">
											<label><strong class="text-forms-pacientes">Motivo de consulta: </strong></label>
											<textarea class="form-control" rows="3" name="motivo_paciente"></textarea>
										</div>
									</div><br>
									<div class="row">
										<div class="col-md-12">
											<label><strong class="text-forms-pacientes">Notas adicionales</strong></label>
											<textarea class="form-control has success" rows="3" name="notas_adicionales_paciente"></textarea>
										</div>
									</div><br>
									<div class="row">
										<div class="col-md-12">
											<input name="guardar" type="submit" class="btn btn-primary btn-lg hoverable" value="Guardar"/>
										</div>
									</div>

								</form>
							</div>
						</div>
					</div><!--/.panel-->
				</div><!--/.col-->
			</div><!--cierra row-->
		</div><!--cierra container-->
	</div>

	<script src="bootstrap/js/jquery-1.11.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/bootstrap-table.js"></script>
	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="js/materialize.min.js"></script>
</body>
</htm>
<?php
    }  
    else {
        header ("Location: index.php");
    }
?>