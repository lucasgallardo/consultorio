<?php
session_start();
include ('conexion.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Valentin</title>
        <link rel="stylesheet" href="css/materialize.min.css">
        <link rel="stylesheet" href="css/index.css">
        <!--
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        -->
    </head>
    <body class="body-background">
        <!--Barra de navegación-->
        <nav>
            <div class="nav-wrapper #039be5 light-blue darken-1"><!--le cambio el color al nav -->
                <a href="index.php" class="brand-logo">&nbsp&nbsp Consultorio Valentin</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="index.php"><i class="material-icons left ">home</i>Inicio</a></li>
                    <li><a href="ayuda.php"><i class="material-icons left">info</i>Ayuda</a></li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="index.php"><i class="material-icons left orange-text">home</i>Inicio</a></li>
                    <li><a href="#"><i class="material-icons left light-blue-text">info</i>Ayuda</a></li>
                </ul>
            </div>
        </nav><!--Fin barra de navegación-->

        <br>
        <?php
        if (isset($_POST['ingresar'])) {
            $usuario_nombre = mysqli_real_escape_string($conexion, $_POST['username']);
            $usuario_clave = mysqli_real_escape_string($conexion, $_POST['password']);
            //$usuario_clave = crypt($usuario_clave,"pass");
            // comprobamos que los datos ingresados en el formulario coincidan con los de la BD
            $sql = mysqli_query($conexion, "SELECT id_usuario, usuario_nombre, usuario_contra FROM usuarios WHERE usuario_nombre='$usuario_nombre' AND usuario_contra='$usuario_clave'") or die(mysqli_error($conexion));

            if ($row = mysqli_fetch_array($sql)) {
                $_SESSION['id_usuario'] = $row['id_usuario']; // creamos la sesion "usuario_id" y le asignamos como valor el campo usuario_id
                $_SESSION['usuario_nombre'] = $row['usuario_nombre']; // creamos la sesion "usuario_nombre" y le asignamos como valor el campo usuario_nombre
                //$_SESSION['usuario_contra'] = $row ['usuario_contra'];
                header("Location: home.php"); //redirecciona a home
            } else {
                //echo "Usuario inexistente";
                ?>
                <div class="container">
                    <div clas="row">
                        <div class="col s12 m12 l12">
                            <div class="card-panel #e53935 red darken-1">
                                <h6>
                                    <span class="white-text center">
                                        <i class="material-icons small">report_problem</i> &nbsp&nbsp <strong>¡ERROR! </strong> El usuario o la contraseña son incorrectos, por favor reintente.
                                    </span>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
                <meta http-equiv="Refresh" content="4; url=index.php">

                <?php
            }
        }
        ?>
        <div class="container">


            <div class="row">
                <div class="col s12 m3"></div>	<!--grid 3 a la izq-->
                <div class="col s12 m6 offset-m3"><!--grid 6 al centro-->
                    <div class="card-panel z-depth-4">
                        <div class="orange-text">
                            <span class="center">
                                <h4>Bienvenido</h4>
                            </span>
                            <p class="center">
                                Por favor inicie sesión o regístrese para continuar
                            </p>
                        </div><br>
                        <form action="" method="POST">
                            <div class="row">   
                                <div class="input-field">
                                    <i class="material-icons prefix">account_circle</i>
                                    <!--user--> <input id="icon_prefix" type="text" class="validate" name="username" id="username" required="" />
                                    <label for="icon_prefix">Usuario:</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field">
                                    <i class="material-icons prefix">lock</i>
                                    <!--user--> <input id="icon_prefix" type="password" class="validate" id="password" name="password" required="" />
                                    <label for="icon_prefix">Password</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m6 center">
                                    <input class="btn waves-effect waves-light orange z-depth-2" type="submit" name="ingresar" value="Ingresar"/>

                                </div>
                                <div class="col s12 m6 center">
                                    <a href="registro.php" class="btn waves-effect waves-light light-blue z-depth-2">Registrarse</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col s12 m3"></div>	<!--grid 3 a la derecha-->
            </div><!--aca termina el row-->

            <hr><br>
            <div class="row">
                <div class="card-panel orange hoverable center">
                    <h5>
                        <span class="white-text">
                            <i class="material-icons small">account_box</i> Iniciar Sesión
                        </span>
                    </h5>
                </div>
            </div>
            <br><br>
            <blockquote>
                Página creada por Pablo Valles.
            </blockquote>
        </div><!--Cierra el container-->


        <script src="js/jquery-2.2.3.min.js"></script>
        <script src="js/materialize.min.js"></script>
        <script>
            $(document).ready(function () {
                $(".button-collapse").sideNav();
            })
        </script>
        <script>
            $(document).ready(function () {
                $('select').material_select();
            });
        </script>
    </body>
</html>