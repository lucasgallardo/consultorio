<?php include('conexion.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Valentin</title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/styles.css" rel="stylesheet">	
        <script src="js/jquery-2.2.3.min.js"></script>
        <script>
            $(function () {
                $("#guardaExamen").click(function () {
                    var url = "examenObstetricoAjax.php";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#formularioExamen").serialize(),
                        success: function (data)
                        {
                            $("#resultadoExamen").html(data);
                            $("#formularioExamen").clearQueue();
                        }
                    });
                    return false;
                });
            });
        </script>
    </head>
    <body>
        <?php include('navbar2.php'); ?>
        <?php include('sidebar2.php'); ?>
        <?php $id_paciente = $_REQUEST["id"]; ?>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main ">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="home.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                    <li><a href="pacientes.php">Pacientes</a></li>
                    <li class="active">Nuevo paciente</li>
                </ol>
            </div><!--/.row-->
            <br />
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 style="color:rgb(48, 165, 255)"><img src="img/icons/1447882687_Add-Male-User.png"> Paciente nuevo - Ficha Obstétrica</h2>
                    </div>

                    <div class="col-md-11">
                        <div class="panel panel-default">
                            <div class="panel-body tabs">
                                <div class="tab-content" style="background-color:#e9ecf2">
                                    <form action="" id="formularioFicha" method="POST">
                                        <legend></legend>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientes">FUM: </strong></label><input class="form-control" type="date" name="fum_pac_first" placeholder=" "/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientes">FPP: </strong></label><input class="form-control" type="date" name="fpp_pac_first" placeholder=" "/>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">Gestas: </strong></label>
                                                    <input class="form-control" type="text" name="gestas_pac" placeholder=""/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">P </strong></label>
                                                    <input class="form-control" type="text" name="partos_pac" placeholder="Partos"/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">CS </strong></label>
                                                    <input class="form-control" type="text" name="cs_pac" placeholder="Cesarias"/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">AB </strong></label>
                                                    <input class="form-control" type="text" name="ab_pac" placeholder=" "/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">EE </strong></label>
                                                    <input class="form-control" type="text" name="ee_pac" placeholder=" "/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">HTAL </strong></label>
                                                    <input class="form-control" type="text" name="htal_pac" placeholder=" "/>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientes">Grupo Sanguineo: </strong></label><input class="form-control" type="text" name="grup_sang_pac" placeholder=" "/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label><strong class="text-forms-pacientes">Marido: </strong></label><input class="form-control" type="text" name="marido_pac" placeholder=" "/>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-xs-1"><span class="text-forms-pacientes">Patología</span></label>
                                                        <div class="col-xs-11">
                                                            <input id="" type="text" class="form-control" placeholder="" name="patolog_pac">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br><br><br>
                                            <div class="col-md-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-xs-1"><span class="text-forms-pacientes">Fármacos</span></label>
                                                        <div class="col-xs-11">
                                                            <input id="" type="text" class="form-control" placeholder="" name="farmacos_pac">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-xs-1"><span class="text-forms-pacientes">PI</span></label>
                                                        <div class="col-xs-11">
                                                            <input id="" type="text" class="form-control" placeholder="" name="pi_pac">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-xs-2"><span class="text-forms-pacientes">Antecedentes</span></label>
                                                        <div class="col-xs-10">
                                                            <input id="" type="text" class="form-control" placeholder="" name="anteced_pac">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><br>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <legend><span style="color:orange">Exámen Obstétrico</span></legend>
                                            <table class="table table-hover table-bordered table-condensed">
                                                <thead>
                                                    <!-- 1) --><th class="info">Fecha</th>
                                                <!-- 2) --><th class="info">EG</th>
                                                <!-- 3) --><th class="info">PA</th>
                                                <!-- 4) --><th class="info">AU</th>
                                                <!-- 5) --><th class="info">TA</th>
                                                <!-- 6) --><th class="info">LCF</th>
                                                <!-- 7) --><th class="info">Edema</th>
                                                <!-- 8) --><th class="info">Datos</th>
                                                <!-- 8) --><th class="info">Agregar</th>
                                                </thead>                                                
                                                <tbody>
                                                    <tr>
                                                <form method="POST" id="formularioExamen">
                                                    <td><input class="form-control" type="date" id="fecha" name="fecha"></td>
                                                    <td><input class="form-control" type="text" name="eg"></td>
                                                    <td><input class="form-control" type="text" name="pa"></td>
                                                    <td><input class="form-control" type="text" name="au"></td>
                                                    <td><input class="form-control" type="text" name="ta"></td>
                                                    <td><input class="form-control" type="text" name="lcf"></td>
                                                    <td><input class="form-control" type="text" name="edema"></td>
                                                    <td><input class="form-control" type="text" name="datos"></td>
                                                    <input type="hidden" class="form-control" id="idPaciente" placeholder="" name="id_paciente" value= "<?php echo $_REQUEST["id"]; ?>" >
                                                    <td>
                                                        <input type="button" value="Agregar" id="guardaExamen" class="btn btn-success btn-lg active">
                                                    </td>                                                    
                                                </form>
                                                </tr>                                                                                                
                                                </tbody>
                                            </table>
                                            <div id="resultadoExamen"></div>
                                        </div>
                                        <legend></legend>
                                    </div><!--Cierra el row-->
                                    <form method="POST" id="formularioEcografias">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <label><strong class="text-forms-pacientes">Ecografías: </strong></label>
                                                    <input class="form-control" type="text" name="notas_eco_pac" placeholder="Notas"/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">TN </strong></label>
                                                    <input class="form-control" type="text" name="tn_eco_pac" placeholder=" "/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">HN </strong></label>
                                                    <input class="form-control" type="text" name="hn_eco_pac" placeholder=" "/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">IPAUD </strong></label>
                                                    <input class="form-control" type="text" name="ipaud_eco_pac" placeholder=" "/>
                                                </div>
                                                <div class="col-md-2">
                                                    <label><strong class="text-forms-pacientes">IPAUI </strong></label>
                                                    <input class="form-control" type="text" name="ipaui_eco_pac" placeholder=" "/>
                                                </div>
                                            </div>
                                        </div><!--Cierra el row--><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-hover table-bordered table-condensed">
                                                    <thead>
                                                        <!-- 1) --><th class="warning">Fecha</th>
                                                    <!-- 2) --><th class="warning">EG</th>
                                                    <!-- 3) --><th class="warning">DBP</th>
                                                    <!-- 4) --><th class="warning">PA</th>
                                                    <!-- 5) --><th class="warning">LF</th>
                                                    <!-- 6) --><th class="warning">Plac</th>
                                                    <!-- 7) --><th class="warning">Grado</th>
                                                    <!-- 8) --><th class="warning">LA</th>
                                                    <!-- 9) --><th class="warning">PEF</th>
                                                    <!-- 10) --><th class="warning">P</th>
                                                    <!-- 11) --><th class="warning">Pres</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fecha_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="eg_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="dbp_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="pa_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="lf_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="plac_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="grado_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="la_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="pef_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="p_eco_1"></td>
                                                            <td><input class="form-control" type="text" name="pres_eco_1"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!--Cierra el row-->
                                    </form>
                                    <form>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><strong style="color:orange">Notas: </strong></label>
                                                    <textarea class="form-control" rows="3" name="notas_imp_eco_pac" placeholder="Notas"></textarea>
                                                </div>
                                                <legend></legend>
                                            </div>
                                        </div><!--Cierra el row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <legend>Laboratorio</legend>
                                                <table class="table table-hover table-bordered table-condensed">
                                                    <thead>
                                                        <!-- 1) --><th class="success">Fecha</th>
                                                    <!-- 2) --><th class="success">HTO</th>
                                                    <!-- 3) --><th class="success">HB</th>
                                                    <!-- 4) --><th class="success">Plaq</th>
                                                    <!-- 5) --><th class="success">GB</th>
                                                    <!-- 6) --><th class="success">TSH</th>
                                                    <!-- 7) --><th class="success">T4L</th>
                                                    <!-- 8) --><th class="success">TPO</th>
                                                    <!-- 9) --><th class="success">TP</th>
                                                    <!-- 10) --><th class="success">TTPK</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fe_lab_1a"></td>
                                                            <td><input class="form-control" type="text" name="hto_lab_1a"></td>
                                                            <td><input class="form-control" type="text" name="hb_lab_1a"></td>
                                                            <td><input class="form-control" type="text" name="plaq_lab_1a"></td>
                                                            <td><input class="form-control" type="text" name="gb_lab_1a"></td>
                                                            <td><input class="form-control" type="text" name="tsh_lab_1a"></td>
                                                            <td><input class="form-control" type="text" name="t4l_lab_1a"></td>
                                                            <td><input class="form-control" type="text" name="tpo_lab_1a"></td>
                                                            <td><input class="form-control" type="text" name="tp_lab_1a"></td>
                                                            <td><input class="form-control" type="text" name="ttpk_lab_1a"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fe_lab_2a"></td>
                                                            <td><input class="form-control" type="text" name="hto_lab_2a"></td>
                                                            <td><input class="form-control" type="text" name="hb_lab_2a"></td>
                                                            <td><input class="form-control" type="text" name="plaq_lab_2a"></td>
                                                            <td><input class="form-control" type="text" name="gb_lab_2a"></td>
                                                            <td><input class="form-control" type="text" name="tsh_lab_2a"></td>
                                                            <td><input class="form-control" type="text" name="t4l_lab_2a"></td>
                                                            <td><input class="form-control" type="text" name="tpo_lab_2a"></td>
                                                            <td><input class="form-control" type="text" name="tp_lab_2a"></td>
                                                            <td><input class="form-control" type="text" name="ttpk_lab_2a"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fe_lab_3a"></td>
                                                            <td><input class="form-control" type="text" name="hto_lab_3a"></td>
                                                            <td><input class="form-control" type="text" name="hb_lab_3a"></td>
                                                            <td><input class="form-control" type="text" name="plaq_lab_3a"></td>
                                                            <td><input class="form-control" type="text" name="gb_lab_3a"></td>
                                                            <td><input class="form-control" type="text" name="tsh_lab_3a"></td>
                                                            <td><input class="form-control" type="text" name="t4l_lab_3a"></td>
                                                            <td><input class="form-control" type="text" name="tpo_lab_3a"></td>
                                                            <td><input class="form-control" type="text" name="tp_lab_3a"></td>
                                                            <td><input class="form-control" type="text" name="ttpk_lab_3a"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>										
                                        </div><!--Cierra el row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-hover table-bordered table-condensed">
                                                    <thead>
                                                        <!-- 1) --><th class="success">Fecha</th>
                                                    <!-- 2) --><th class="success">Glu</th>
                                                    <!-- 3) --><th class="success">Ur</th>
                                                    <!-- 4) --><th class="success">Creat</th>
                                                    <!-- 5) --><th class="success">Uric</th>
                                                    <!-- 6) --><th class="success">LDH</th>
                                                    <!-- 7) --><th class="success">GOT</th>
                                                    <!-- 8) --><th class="success">GPT</th>
                                                    <!-- 9) --><th class="success">BD</th>
                                                    <!-- 10) --><th class="success">BI</th>
                                                    <!-- 11) --><th class="success">BT</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fe_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="glu_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="ur_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="creat_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="uric_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="ldh_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="got_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="gpt_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="bd_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="bi_lab_1b"></td>
                                                            <td><input class="form-control" type="text" name="bt_lab_1b"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fe_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="glu_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="ur_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="creat_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="uric_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="ldh_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="got_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="gpt_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="bd_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="bi_lab_2b"></td>
                                                            <td><input class="form-control" type="text" name="bt_lab_2b"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fe_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="glu_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="ur_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="creat_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="uric_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="ldh_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="got_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="gpt_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="bd_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="bi_lab_3b"></td>
                                                            <td><input class="form-control" type="text" name="bt_lab_3b"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!--Cierra el row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-hover table-bordered table-condensed">
                                                    <thead>
                                                        <!-- 1) --><th class="success">Fecha</th>
                                                    <!-- 2) --><th class="success">Colesterol</th>
                                                    <!-- 3) --><th class="success">HDL</th>
                                                    <!-- 4) --><th class="success">LDL</th>
                                                    <!-- 5) --><th class="success">TG</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fe_lab_1c"></td>
                                                            <td><input class="form-control" type="text" name="coles_lab_1c"></td>
                                                            <td><input class="form-control" type="text" name="hdl_lab_1c"></td>
                                                            <td><input class="form-control" type="text" name="ldl_lab_1c"></td>
                                                            <td><input class="form-control" type="text" name="tg_lab_1c"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fe_lab_2c"></td>
                                                            <td><input class="form-control" type="text" name="coles_lab_2c"></td>
                                                            <td><input class="form-control" type="text" name="hdl_lab_2c"></td>
                                                            <td><input class="form-control" type="text" name="ldl_lab_2c"></td>
                                                            <td><input class="form-control" type="text" name="tg_lab_2c"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input class="form-control" type="date" name="fe_lab_3c"></td>
                                                            <td><input class="form-control" type="text" name="coles_lab_3c"></td>
                                                            <td><input class="form-control" type="text" name="hdl_lab_3c"></td>
                                                            <td><input class="form-control" type="text" name="ldl_lab_3c"></td>
                                                            <td><input class="form-control" type="text" name="tg_lab_3c"></td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                                <legend></legend>
                                            </div>
                                        </div><!--Cierra el row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label><strong class="text-forms-pacientes">SEROLOGÍA </strong></label>
                                                <div class="form-inline">
                                                    <label><strong class="text-forms-pacientes">1T: </strong></label>
                                                    <input class="form-control" type="text" name="" placeholder=" "/><br><br>
                                                    <label><strong class="text-forms-pacientes">2T: </strong></label>
                                                    <input class="form-control" type="text" name="" placeholder=" "/><br><br>
                                                    <label><strong class="text-forms-pacientes">3T: </strong></label>
                                                    <input class="form-control" type="text" name="" placeholder=" "/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">CMV: </strong></label><input class="form-control" type="text" name="cmv_pac" placeholder=" "/>
                                                </div>
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">Rubeola: </strong></label><input class="form-control" type="text" name="rubeola_pac" placeholder=" "/>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <legend>Urocultivo</legend>
                                                <ul>
                                                    <li>
                                                        <label><strong class="text-forms-pacientes">1T: </strong></label>
                                                        <input class="form-control" type="text" name="uro_1t_pac" placeholder=" "/>
                                                    </li><br>
                                                    <li>
                                                        <label><strong class="text-forms-pacientes">2T: </strong></label>
                                                        <input class="form-control" type="text" name="uro_2t_pac" placeholder=" "/>
                                                    </li><br>
                                                    <li>
                                                        <label><strong class="text-forms-pacientes">3T: </strong></label>
                                                        <input class="form-control" type="text" name="uro_3t_pac" placeholder=" "/>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-4"></div>
                                                <div class="col-md-4"><br><br><img src="img/icons/add8.png"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"><legend></legend>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label><strong class="text-forms-pacientes">CTOG: </strong></label><br>
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label class="sr-only" ></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">EG: </div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="" name="eg_input_1a">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input class="form-control" type="text" name="eg_input_2a">
                                                    </div>
                                                </div><br><br>
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label class="sr-only" ></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">EG: </div>
                                                            <input name="eg_input_1b" type="text" class="form-control" id="exampleInputAmount" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input class="form-control" type="text" name="eg_input_2b">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label><strong class="text-forms-pacientes">Notas: </strong></label>
                                                <br>
                                                <textarea class="form-control" rows="6" name="notas_ctog_pac"></textarea>
                                            </div>
                                        </div><!--cierra row--><br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="alert alert-warning">
                                                    <div class="form-group">
                                                        <label><strong>Vacunas: </strong></label>
                                                        <img src="img/icons/2.png">
                                                        <textarea class="form-control" rows="6" name="vacunas_pac"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="alert alert-success">
                                                    <div class="form-inline">
                                                        <label><strong>Coombs: </strong></label>
                                                        <img src="img/icons/2.png"><br><br>
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" value="" name="ch_20_pac"> 20 <i class="glyphicon glyphicon-arrow-right"></i>
                                                                    <input type="text" name="input_ch20" class="form-control">
                                                                </label><br><br>
                                                            </div>
                                                        </div><br>
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="ch_24_pac" type="checkbox" value=""> 24 
                                                                </label>
                                                                <i class="glyphicon glyphicon-arrow-right"></i> 
                                                                <input type="text" name="input_ch24" class="form-control">
                                                            </div>
                                                        </div><br><br>
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="ch_28_pac" type="checkbox" value=""> 28 
                                                                </label>
                                                                <i class="glyphicon glyphicon-arrow-right"></i>
                                                                <input type="text" name="input_ch28" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label><strong class="text-forms-pacientes">Streptococo: </strong></label>
                                                <input class="form-control" type="text" name="strep_pac" placeholder=""/>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label><strong class="text-forms-pacientes">Psicoprofilaxis: </strong></label>
                                                <input class="form-control" type="text" name="psicoprof_pac" placeholder=""/>
                                            </div>
                                            <div class="col-md-6">
                                                <label><strong class="text-forms-pacientes">Maduración: </strong></label>
                                                <input class="form-control" type="text" name="mad_pac" placeholder=""/>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label><strong class="text-forms-pacientes">Lactancia: </strong></label>
                                                <input class="form-control" type="text" name="lactan_pac" placeholder=""/>
                                            </div>
                                            <div class="col-md-6">
                                                <label><strong class="text-forms-pacientes">NST: </strong></label>
                                                <input class="form-control" type="text" name="nst_pac" placeholder=""/>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">Doppler: </strong></label>
                                                    <textarea class="form-control" rows="5" name="doppler_pac" placeholder=""></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">Ecocardiograma: </strong></label>
                                                    <textarea class="form-control" rows="5" name="ecocard_pac" placeholder=""></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><strong class="text-forms-pacientes">Notas: </strong></label>
                                                    <textarea class="form-control" rows="5" name="notas_muy_imp_pac" placeholder=""></textarea>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="submit" class="btn btn-success btn-lg btn-block hoverable" name="guardar5" value="Guardar"/>
                                            </div>
                                            <div class="col-md-9"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!--/.panel-->
                    </div><!--/.col-->
                </div><!--cierra row-->
            </div><!--cierra container-->
        </div>

        <script src="bootstrap/js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap-table.js"></script>
        <script src="js/materialize.min.js"></script>
    </body>
</htm>
