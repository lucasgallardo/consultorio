<?php
session_start();
include ("../conexion.php");
date_default_timezone_set('America/Argentina');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
<style>
    p{
        font-size: 13px;
    }
    ul{
        list-style-type: circle;
        font-size: 10px;
    }
</style>
</head>
<body>

	<?php
		$fechaDeHoy = date("d/m/y h:m");			
		$nombre = "Analítica2.doc";
		
		header('Content-type: application/vnd.ms-word');
		header("Content-Disposition: attachment; filename=$nombre");
		header("Pragma: no-cache");
		header("Expires: 0");
	?>




<?php
$recibir = $_REQUEST['accion'];

$ver = mysqli_query($conexion, "SELECT * from pacientes where id_paciente='$recibir'");
if ($reg = mysqli_fetch_array($ver)){
    
?>

<h2 style="text-align:center">DRA. FABIANA VALLES</h2>
<p style="text-align:center">MAT. 1997</p>
<h5 style="text-align:center">GINECOLOGIA-OBSTETRICIA</h5>
<hr>
<h4><u>Analítica</u></h4>
Fecha: <?php echo $fechaDeHoy; ?>
<br>
<p><u>Apellido:</u> <?php echo $reg['apellido_pac'] ?></p>
<p><u>Nombres:</u> <?php echo $reg['nombre_pac'] ?></p>
<p><u>Obra Social:</u> <?php echo $reg['obsocial_pac'] ?></p>
<p><u>Número de obra Social:</u> <?php echo $reg['nro_obsocial_pac'] ?></p>
<p><u>Plan Obra social:</u> <?php echo $reg['plan_obsocial_pac'] ?></p>
<ul>
    <li>Hemograma</li>
    <li>Recuento de plaquetas</li>
    <li>Fibrinógeno</li>
    <li>Glucemia</li>
    <li>Uremia</li>
    <li>Uricemia</li>
    <li>Creatininemia</li>
    <li>T.P.</li>
    <li> - TTPK</li>
    <li>GOT</li>
    <li>GPT</li>
    <li>FAL</li>
    <li>LDH</li>
    <li>BILIRRUBINA (D-I-T)</li>
    <li>Orina Completa</li>
    <li>Colesterol total </li>
    <li>LDL</li>
    <li>HDL</li>
    <li>Trigliceridos</li>
    <li>TSH</li>
    <li>T4 LIBRE</li>
</ul>
<br>
<h5><u>DIAG: control hipotiroidismo</u></h5>
<br><br><br>
<hr>
<h5 style="text-align:center; width:30px">
CDTE FOSSA N° 98  ESQ.SERU  (B°BOMBAL) CIUDAD					
SAN MARTIN N° 1440 - UNIDAD 2 -GODOY CRUZ						
TELEF.0261-4246059   Y     0261-4245182						
TELEF.MOVIL 154-605712						
</h5> 


<?php }
?>
</body>
</html>